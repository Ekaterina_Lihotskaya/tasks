import java.util.logging.Logger;

/**
 * Simple class is intended for demonstration of work ant
 */
public final class HelloAnt {
    private HelloAnt() {
    }

    private static final Logger LOGGER = Logger.getLogger(HelloAnt.class.getName());

    /**
     * start of the work
     * @param args command line argument
     */
    public static void main(final String[] args) {
        LOGGER.info("Hello, I'm Ant");
    }
}