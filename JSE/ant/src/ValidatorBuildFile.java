import org.apache.tools.ant.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Check for validity of ant files
 */

public class ValidatorBuildFile extends Task {
    private static final Logger LOGGER = Logger.getLogger(ValidatorBuildFile.class.getName());

    private boolean checkDepends;
    private boolean checkDefault;
    private boolean checkNames;
    private final List<Buildfile> buildfiles = new ArrayList<>();

    private final String REGEX = "^[a-zA-Z]+[a-zA-Z-]*$";
    private final Pattern NAMEE_PATTERN = Pattern.compile(REGEX);

    /**
     * Check for validity of ant files according to the specified flags
     */
    public void execute() {

        for (Buildfile buildfile : buildfiles) {
            Project project = getProject(buildfile.getLocation());
            if (checkDepends) {
                methodCheckDepends(project);
            }
            if (checkDefault) {
                methodCheckDefault(project);
            }

            if (checkNames) {
                methodCheckNames(project);
            }

        }
        LOGGER.info("Method execute is done.");
    }

    private Project getProject(final File location) {
        Project project = new Project();
        project.init();
        ProjectHelper helper = ProjectHelper.getProjectHelper();
        helper.parse(project, location);
        LOGGER.info("Method getProject is done.");
        return project;
    }


    private void methodCheckDepends(final Project project) {
        Map<String, Target> listTargets = project.getTargets();
        String nameDefaultTarget = project.getDefaultTarget();
        for (Map.Entry<String, Target> target : listTargets.entrySet()) {
            String nameTarget = target.getKey();
            if (target.getValue().getDependencies().hasMoreElements() && !(nameDefaultTarget.equals(nameTarget))) {
                throw new BuildException("Dependens of target " + nameTarget + " is not correct.");
            }
        }
        LOGGER.info("Method methodcheckdepends is done. All dependens are correct.");
    }

    private void methodCheckDefault(final Project project) {
        String nameDefaultTarget = project.getDefaultTarget();
        if ("".equals(nameDefaultTarget)) {
            throw new BuildException("Default target is not set");
        }
        Map<String, Target> listTargets = project.getTargets();
        for (Map.Entry<String, Target> target : listTargets.entrySet()) {
            String nameTarget = target.getKey();
            if (nameTarget.equals(nameDefaultTarget)) {
                LOGGER.info("Method methodcheckdefault is done. Default target exists.");
                return;
            }
        }
        throw new BuildException("Default target does not exist");
    }

    private void methodCheckNames(final Project project) {

        Map<String, Target> listTargets = project.getTargets();
        for (Map.Entry<String, Target> target : listTargets.entrySet()) {
            String nameTarget = target.getKey();
            Matcher matcher = NAMEE_PATTERN.matcher(nameTarget);
            if (!matcher.matches() && !"".equals(nameTarget)) {
                throw new BuildException("Target " + nameTarget + " has a wrong name");
            }
        }
        LOGGER.info("Method methodchecknames is done. All targets have valid names.");

    }

    /**
     * Set flag checkDepends
     *
     * @param checkDepends setting the check flag: targets with depends are used instead of "main" point
     */
    public void setCheckDepends(final boolean checkDepends) {
        this.checkDepends = checkDepends;
    }

    /**
     * Set flag checkDefault
     *
     * @param checkDefault setting the check flag: project contains default attribute
     */
    public void setCheckDefault(final boolean checkDefault) {
        this.checkDefault = checkDefault;
    }

    /**
     * Set flag checkNames
     *
     * @param checkNames setting the check flag: names contains only letters with '-'
     */
    public void setCheckNames(final boolean checkNames) {
        this.checkNames = checkNames;
    }

    /**
     * Create new buildfile and add it in the list buildfiles
     *
     * @return new buildfile
     */
    public Buildfile createBuildfile() {
        Buildfile buildfile = new Buildfile();
        buildfiles.add(buildfile);
        return buildfile;
    }

    /**
     * class Buildfile
     */
    public class Buildfile {
        /**
         * Initializes a newly created Buildfile object
         */
        public Buildfile() {
        }

        private File location;

        /**
         * set Buildfile location
         *
         * @param location set Buildfile location
         */
        public void setLocation(final File location) {
            this.location = location;
        }

        /**
         * get Buildfile location
         *
         * @return get Buildfile location
         */
        public File getLocation() {
            return location;
        }
    }
}

