package by.training;

import by.training.impl.Cat;
import by.training.impl.Child;
import org.junit.Assert;
import org.junit.Test;


public class AnalyzerTest {

    @Test
    public void test(){
        Cat cat1 = new Cat("Gar");
        Child child1 = new Child("Tom", 5, cat1);
        Child child2 = new Child("Tom", 5, cat1);
        Assert.assertTrue(Analyzer.equalObjects(child1, child2));


        child1 = new Child("Tom", 5, cat1);
        child2 = new Child("Jack", 5, cat1);
        Assert.assertFalse(Analyzer.equalObjects(child1, child2));

        child1 = new Child("Tom", 5, cat1);
        child2 = new Child("Tom", 10, cat1);
        Assert.assertFalse(Analyzer.equalObjects(child1, child2));


        Cat cat2 = new Cat("Gar");
        child1 = new Child("Tom", 5, cat1);
        child2 = new Child("Tom", 5, new Cat("Gar"));
        Assert.assertFalse(Analyzer.equalObjects(child1, child2));
    }
}