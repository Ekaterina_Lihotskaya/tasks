import by.training.factories.InstanceFactory;
import by.training.ifaces.IPlay;
import by.training.impl.Cat;
import by.training.impl.Child;

public class TestReflection {

    public static void main(String[] args) throws Exception {
        IPlay cat = (IPlay) InstanceFactory.getInstanceOf(Cat.class);
        IPlay child = (IPlay) InstanceFactory.getInstanceOf(Child.class);
        cat.play();
        child.play();
    }
}