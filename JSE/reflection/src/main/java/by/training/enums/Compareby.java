package by.training.enums;

/**
 * Enum contains criteria to compare fields
 */
public enum CompareBy {
    /**
     * Determine objects equality by reference
     */
    REFERENCE,
    /**
     * Determine objects equality by value (state)
     */
    VALUE
}