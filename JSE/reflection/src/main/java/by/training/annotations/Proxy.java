package by.training.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The annotation may appear on classes and declares
 * that a holder class can become proxy.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)

public @interface Proxy {
    /**
     * @return class name for using as handler
     */
    String invocationHandler();
}
