package by.training.annotations;

import by.training.enums.CompareBy;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The annotation may appear on fields and declares
 * that a field is used to compare objects.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)

public @interface Equal {
    /**
     * Defines how this field will be compared by reference or by value
     * @return true referance or value
     */
    CompareBy comparaBy() default CompareBy.VALUE;
}