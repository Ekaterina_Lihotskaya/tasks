package by.training.factories;

import by.training.annotations.Proxy;
import java.lang.reflect.InvocationHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The class is used to get an object instance for a class provided as attribute
 */
public class InstanceFactory  {
    private static final Logger LOGGER = Logger.getLogger(InstanceFactory.class.getName());

    /**
     * A method to get an object for the specified class
     * @param clazz specified class
     * @return proxy object or common instance
     */
    public static Object getInstanceOf(final Class<?> clazz) throws Exception  {
        Object instance;
        try {
            if (clazz.isAnnotationPresent(Proxy.class)) {
                instance = getProxyInstance(clazz);
            } else {
                instance = clazz.newInstance();
            }
            return instance;
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new Exception();
        }
    }

    private static Object getProxyInstance(final Class<?> clazz) throws InstantiationException,
            IllegalAccessException, ClassNotFoundException {
        Proxy proxy = clazz.getAnnotation(Proxy.class);
        String invocationHandler = proxy.invocationHandler();
        InvocationHandler invocationHandlerInstance =
                (InvocationHandler) Class.forName(invocationHandler).newInstance();
        return java.lang.reflect.Proxy.newProxyInstance(clazz.getClassLoader(),
                clazz.getInterfaces(), invocationHandlerInstance);
    }
}
