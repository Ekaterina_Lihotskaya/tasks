package by.training.handlers;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * Implementing InvocationHandler
 */
public class PlayHandler implements InvocationHandler {
    @Override
    public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
        System.out.println("PlayHandler won't let me play.");
        return null;
    }
}