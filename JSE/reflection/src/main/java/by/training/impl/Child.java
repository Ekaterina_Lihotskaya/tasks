package by.training.impl;

import by.training.annotations.Equal;
import by.training.enums.CompareBy;
import by.training.ifaces.IPlay;

/**
 * A class representing a child, implements an interface IPlay
 */
public class Child implements IPlay {
    @Equal(comparaBy = CompareBy.REFERENCE)
    private String name;
    @Equal(comparaBy = CompareBy.VALUE)
    private int age;
    @Equal(comparaBy = CompareBy.REFERENCE)
    private Cat cat;

    @Override
    public void play() {
        System.out.println("I'm Tom. I'm playing with my friends.");
    }

    /**
     * Initializes a newly created Child object so that it represents
     * an unknown child
     */
    public Child() {
    }

    /**
     * Initializes a newly created Child object so that it represents
     * the child
     *
     * @param name name child
     * @param age  age child
     * @param cat  a cat belonging to a child
     */
    public Child(final String name, final int age, final Cat cat) {
        this.name = name;
        this.age = age;
        this.cat = cat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Child child = (Child) o;

        if (age != child.age) return false;
        if (name != null ? !name.equals(child.name) : child.name != null) return false;
        return cat != null ? cat.equals(child.cat) : child.cat == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + age;
        result = 31 * result + (cat != null ? cat.hashCode() : 0);
        return result;
    }
}