package by.training.impl;

import by.training.annotations.Proxy;
import by.training.ifaces.IPlay;

/**
 * A class representing a cat, implements an interface IPlay
 */
@Proxy(invocationHandler = "by.training.handlers.PlayHandler")
public class Cat implements IPlay {
    private String name;

    @Override
    public void play() {
        System.out.println("Hi, I'm cat, I'm playing with a ball");
    }

    /**
     * Initializes a newly created Cat object so that it represents
     * an unknown cat
     */
    public Cat() {
    }

    /**
     * Initializes a newly created Cat object so that it represents
     * cat with name
     * @param name name cat
     */
    public Cat(final String name) {
        this.name = name;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Cat cat = (Cat) o;

        return name != null ? name.equals(cat.name) : cat.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}