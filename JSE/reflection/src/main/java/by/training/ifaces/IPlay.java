package by.training.ifaces;

/**
 *  Determines the behavior of the game
 */
public interface IPlay {
    /**
     * Describes the rules of the game
     */
    void play();
}