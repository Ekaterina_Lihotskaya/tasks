package by.training;

import by.training.annotations.Equal;
import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The class expects to get objects and makes fields scanning.
 * Returns true if the objects are equal
 */
public class Analyzer {
    private static final Logger LOGGER = Logger.getLogger(Analyzer.class.getName());

    /**
     * Uses a @Equal annotation to make a decision of including a field in the result
     * @param o1 first object
     * @param o2 second object
     * @return true if the objects are equal
     */
    public static boolean equalObjects(final Object o1, final Object o2) {
        if (!equalGeneralOfObjects(o1, o2)) {
            return false;
        }

        Field[] declaredFields = o1.getClass().getDeclaredFields();
        try {
            for (Field field : declaredFields) {
                if (field.isAnnotationPresent(Equal.class)) {
                    if (!equalField(o1, o2, field)) {
                        return false;
                    }
                }
            }
        } catch (IllegalAccessException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            return false;
        }
        return true;

    }

    private static boolean equalGeneralOfObjects(final Object o1, final Object o2) {
        if (o1 == o2) {
            return true;
        }
        if (o1 == null || o2 == null) {
            return false;
        }
        return o1.getClass() == o2.getClass();
    }

    private static boolean equalField(final Object o1, final Object o2,
                                      final Field field) throws IllegalAccessException {
        Equal equalField = field.getAnnotation(Equal.class);
        field.setAccessible(true);
        Object fieldO1 = field.get(o1);
        Object fieldO2 = field.get(o2);
        boolean isEqual = true;
        switch (equalField.comparaBy()) {
            case REFERENCE:
                if (!field.getType().isPrimitive()) {
                    if (fieldO1 != fieldO2) {
                        isEqual = false;
                    }
                    break;
                }
            case VALUE:
                if (fieldO1 == null || !fieldO1.equals(fieldO2)) {
                    isEqual = false;
                }
                break;
            default: throw new IllegalAccessException();
        }
        field.setAccessible(false);
        return isEqual;
    }
}