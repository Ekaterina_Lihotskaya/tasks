package by.training.impl;

import by.training.ifaces.ICache;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * LFU is a cache eviction algorithm called least frequently used cache.
 *
 * @param <K>
 * @param <V>
 */
public class LRUCache<K, V> implements ICache<K, V> {
    private Map<K, V> map;

    /**
     * Constractor LRUCache
     *
     * @param maxSize max size of cache
     */

    public LRUCache(final int maxSize) {
        map = Collections.synchronizedMap(new LinkedHashMap<K, V>(maxSize + 1, 1, true) {
            @Override
            protected boolean removeEldestEntry(final Map.Entry<K, V> eldest) {
                return size() > maxSize;
            }
        });
    }

    @Override
    public void put(final K key, final V value) {
        map.put(key, value);
    }

    @Override
    public V get(final K key) {
        return map.get(key);

    }
}