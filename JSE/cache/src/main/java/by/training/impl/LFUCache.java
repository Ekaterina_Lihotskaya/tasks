package by.training.impl;

import by.training.ifaces.ICache;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * Implementation LRUCache
 *
 * @param <K> key
 * @param <V> key value
 */
public class LFUCache<K, V> implements ICache<K, V> {
    private final int maxSize;
    private Map<K, Note> map;
    private List<ConcurrentLinkedDeque<K>> listFrequencies;
    private static double EVICTION_FACTOR = 0.75;
	private int countDeleteElements;

    /**
     * Constractor LFUCache
     *
     * @param maxSize max size of cache
     */
    public LFUCache(final int maxSize) {
        this.maxSize = maxSize;
        map = new HashMap<K, Note>(maxSize+1, 1);
        countDeleteElements = (int) Math.round(maxSize * EVICTION_FACTOR);
        createListFrequencies(maxSize);
    }

    private void createListFrequencies(final int maxSize) {
        listFrequencies = new ArrayList<>(maxSize);
        for (int i = 0; i < maxSize; i++) {
            listFrequencies.add(new ConcurrentLinkedDeque<K>());
        }
    }

    /**
     * @param key   - may not be null
     * @param value - may not be null
     */
    public synchronized void put(final K key, final V value) {
        if (key == null || value == null) {
            throw new IllegalArgumentException("Key and value cannot be null");
        }
        if (map.containsKey(key)) {
            map.get(key).setValue(value);
            return;
        }

        checkSizeCache();
        map.put(key, new Note(value));
        listFrequencies.get(0).add(key);
    }

    /**
     * @param key - may not be null
     * @return the value associated to the given key or null
     */
    public synchronized V get(final K key) {
        Note note = map.get(key);
        V value = null;
        if (note != null) {
            moveKeyToNewListFrequency(key);
            value = note.getValue();
        }
        return value;
    }

    private synchronized void moveKeyToNewListFrequency(final K key) {
        int frequency = map.get(key).frequency;
        listFrequencies.get(frequency).remove(key);
        if (frequency == maxSize - 1) {
            listFrequencies.get(frequency).add(key);
        }
        else {
            map.get(key).setFrequency(++frequency);
            listFrequencies.get(frequency).add(key);
        }
    }

    private synchronized void checkSizeCache(){
        int count = 0;
        if (map.size() >= maxSize) {
            for (int i = 0; i < maxSize; i++) {
                while (!listFrequencies.get(i).isEmpty()) {
                    System.out.println(Thread.currentThread().getName() + i + " "+ map.remove(listFrequencies.get(i).poll()) + map.size());
                    count++;
                    if(count == countDeleteElements){
                    return;
                    }
                }
            }
        }
    }

    private class Note {
        private int frequency = 0;
        private V value;

        Note(final V value) {
            this.value = value;
        }

        private V getValue() {
            return value;
        }

        private void setFrequency(final int frequency) {
            this.frequency = frequency;
        }

        private void setValue(final V value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "Note{" + "frequency=" + frequency + ", value=" + value + '}';
        }
    }

}