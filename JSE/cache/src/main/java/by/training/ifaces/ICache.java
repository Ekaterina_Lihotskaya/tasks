package by.training.ifaces;

/**
 * Contract for cache
 *
 * @param <K> key
 * @param <V> key value
 */
public interface ICache<K, V> {
    /**
     * Add key and value to the cache
     *
     * @param key   key
     * @param value key value
     */
    void put(K key, V value);

    /**
     * Get value in accoding with key from the cache
     *
     * @param key key
     * @return key value
     */
    V get(K key);
}