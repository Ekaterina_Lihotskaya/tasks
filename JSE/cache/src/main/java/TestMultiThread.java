import by.training.ifaces.ICache;
import by.training.impl.LFUCache;
import by.training.impl.LRUCache;

/**
 * testing the cache
 */
public class TestMultiThread {

    /**
     * start of the work
     * @param args command line argument
     */
    public static void main(final String[] args) {
        ICache<Integer, Integer> cache = new LFUCache<>(10);
      //  ICache<Integer, Integer> cache = new LRUCache<>(10);
        runThreadWrite(cache, 1);
        runThreadWrite(cache, 5);
        runThreadRead(cache, 8);
        runThreadRead(cache, 1);
    }

    private static void runThreadWrite(final ICache<Integer, Integer> cache, final int initial) {
        new Thread(() -> {
            for (int i = initial; i < initial + 100; i++) {
                cache.put(i, i + 10);
                System.out.println(Thread.currentThread().getName());
            }
        }).start();
    }

    private static void runThreadRead(final ICache<Integer, Integer> cache, final int initial) {
        new Thread(() -> {
            for (int i = initial; i < initial + 100; i++) {
                System.out.println(Thread.currentThread().getName() + " " + cache.get(i));
            }
        }).start();
    }
}