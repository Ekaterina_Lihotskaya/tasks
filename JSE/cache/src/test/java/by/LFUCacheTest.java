package by;

import by.training.ifaces.ICache;
import by.training.impl.LFUCache;
import org.junit.Assert;
import org.junit.Test;

public class LFUCacheTest {
    @Test
    public void addKeyAndValue() {
        ICache<String, String> expected = new LFUCache<>(4);
        expected.put("1", "1");
        expected.put("2", "2");
        expected.put("3", "3");
        expected.put("4", "4");
        expected.get("2");
        expected.put("5", "5");
        Assert.assertNull(expected.get("1"));
        Assert.assertEquals(expected.get("2"), "2");
        Assert.assertNull(expected.get("3"));
        Assert.assertNull(expected.get("4"));
        expected.get("2");
        expected.put("11", "11");
        expected.put("1", "1");
        expected.put("10", "10");
        Assert.assertNull(expected.get("5"));
        Assert.assertNull(expected.get("11"));
        Assert.assertEquals(expected.get("2"), "2");
        Assert.assertNull(expected.get("1"));
    }
}