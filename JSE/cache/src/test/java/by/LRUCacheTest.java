package by;

import by.training.ifaces.ICache;
import by.training.impl.LRUCache;
import org.junit.Assert;
import org.junit.Test;

public class LRUCacheTest {
    @Test
    public void addKeyAndValue(){
        ICache<String, String> expected = new LRUCache<>(3);
        expected.put("1", "5");
        expected.put("2", "8");
        expected.put("3", "8");
        expected.put("4", "5");
        expected.put("4", "8");
        expected.put("1", "5");
        Assert.assertEquals(expected.get("1"),"5");
        Assert.assertEquals(expected.get("4"),"8");
        Assert.assertEquals(expected.get("3"),"8");
        Assert.assertNull(expected.get("2"));

    }
}