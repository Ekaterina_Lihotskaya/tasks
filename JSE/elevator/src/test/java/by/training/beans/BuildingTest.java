package by.training.beans;

import org.junit.Test;

import static org.junit.Assert.*;


public class BuildingTest {
    @Test
    public void testCreatelistStories() throws Exception {
        int expectedStoriesNumber = 10;
        Building building = new Building(new Elevator(5), expectedStoriesNumber);
        int actual = building.getListStories().size();
        assertEquals(expectedStoriesNumber, actual);
    }
}