package by.training.imp;

import by.training.beans.Building;
import by.training.beans.Elevator;
import by.training.beans.Passenger;
import by.training.beans.Storey;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.concurrent.CountDownLatch;

import static org.junit.Assert.*;

public class ConsistentControllerTest {
    private Storey currentStorey;
    private Passenger currentPassengerOne;
    private Passenger currentPassengerTwo;
    private Elevator elevator;
    private Building building;
    private ConsistentController controller;

    @Before
    public void setUp() throws Exception {
        currentStorey = new Storey(3);
        currentPassengerOne = new Passenger(5, new Storey(1), currentStorey);
        currentPassengerTwo = new Passenger(1, new Storey(2), currentStorey);
        elevator = new Elevator(10);
        building = new Building(elevator, 5);
        controller = new ConsistentController(building, new CountDownLatch(3));
    }

    @Test
    public void exitEnterPassengers() throws Exception {
        ConsistentController controllerOne = Mockito.mock(ConsistentController.class);
        elevator.getEvelvatorContainer().add(currentPassengerOne);
        Mockito.doCallRealMethod().when(controllerOne).exitEnterPassengers(elevator, currentStorey);
        controllerOne.exitEnterPassengers(elevator, currentStorey);
        Mockito.verify(controllerOne, Mockito.times(1)).enterPassengers(elevator, currentStorey);
        Mockito.verify(controllerOne, Mockito.times(1)).exitPassengers(elevator, currentStorey);
    }

    @Test
    public void deboadingOfPassengers() throws Exception {

        assertTrue(elevator.getEvelvatorContainer().isEmpty());
        elevator.getEvelvatorContainer().add(currentPassengerOne);
        assertTrue(elevator.getEvelvatorContainer().size() == 1);

        elevator.getEvelvatorContainer().add(currentPassengerTwo);
        assertTrue(elevator.getEvelvatorContainer().size() == 2);

        controller.setCurrentPassenger(currentPassengerOne);
        controller.deboadingOfPassengers(currentStorey);
        assertTrue(elevator.getEvelvatorContainer().size() == 1);
        assertTrue(currentStorey.getArrivalStoryContainer().size() == 1);

        controller.setCurrentPassenger(currentPassengerTwo);
        controller.deboadingOfPassengers(currentStorey);
        assertTrue(elevator.getEvelvatorContainer().isEmpty());
        assertTrue(currentStorey.getArrivalStoryContainer().size() == 2);

    }

    @Test
    public void boadingOfPassengers() throws Exception {
        currentPassengerOne.setDepartureStorey(currentStorey);
        currentPassengerTwo.setDepartureStorey(currentStorey);

        assertTrue(elevator.getEvelvatorContainer().isEmpty());
        currentStorey.getDispatchStoryContainer().add(currentPassengerOne);
        assertTrue(currentStorey.getDispatchStoryContainer().size() == 1);

        currentStorey.getDispatchStoryContainer().add(currentPassengerTwo);
        assertTrue(currentStorey.getDispatchStoryContainer().size() == 2);

        controller.setCurrentPassenger(currentPassengerOne);
        controller.boadingOfPassengers(currentStorey);
        assertTrue(elevator.getEvelvatorContainer().size() == 1);
        assertTrue(currentStorey.getDispatchStoryContainer().size() == 1);

        controller.setCurrentPassenger(currentPassengerTwo);
        controller.boadingOfPassengers(currentStorey);
        assertTrue(elevator.getEvelvatorContainer().size() == 2);
        assertTrue(currentStorey.getDispatchStoryContainer().isEmpty());
    }

}