package by.training.imp;

import by.training.beans.Building;
import by.training.beans.Elevator;
import by.training.beans.Passenger;
import by.training.beans.Storey;
import by.training.enums.ElevatorState;
import by.training.ifaces.TransportationControllerAbstract;
import by.training.ifaces.TransportationTaskAbstract;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;

import static org.junit.Assert.*;

public class TransportationTaskTest {
    private Elevator elevator;
    private TransportationTaskAbstract task;
    private TransportationControllerAbstract controller;
    private Passenger passenger;


    @Before
    public void setUp() throws Exception {
        passenger = new Passenger(1, new Storey(1), new Storey(5));
        elevator = new Elevator(10);
        CountDownLatch latch = new CountDownLatch(0);
        controller = new ConsistentController(new Building(elevator, 5), latch);
        task = new TransportationTask(passenger, controller, elevator, latch);
    }

    @Test
    public void movePassenger() throws Exception {
    }
    @Test
    public void isConditionEnter() throws Exception {
        elevator.setElevatorState(ElevatorState.STOP_INPUT);
        assertTrue(task.isConditionEnter());

        controller.setElevatorState(ElevatorState.MOVE_DOWN);
        assertFalse(task.isConditionEnter());

        controller.setElevatorState(ElevatorState.MOVE_UP);
        elevator.setElevatorState(ElevatorState.STOP_OUTPUT);
        assertFalse(task.isConditionEnter());

        elevator.setElevatorState(ElevatorState.STOP_INPUT);
        passenger.setDepartureStorey(new Storey(7));
        assertFalse(task.isConditionEnter());
    }

    @Test
    public void isConditionExit() throws Exception {
        assertTrue(task.isConditionExit());

        elevator.setElevatorState(ElevatorState.STOP_INPUT);
        assertFalse(task.isConditionExit());

    }

}