package by.training.services;

import by.training.beans.Building;
import by.training.beans.Elevator;
import by.training.beans.Passenger;
import by.training.beans.Storey;
import by.training.enums.TransportationState;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;


public class ValidationServiceTest {
    private Building emptyBuilding;
    private Building buildingWithPassengers;
    private ArrayList<Passenger> listPassengers = new ArrayList<>();;
    private Passenger passenger = new Passenger(1, new Storey(2), new Storey(5));

    @Before
    public void setUp() throws Exception {
        Elevator elevator = new Elevator(5);
        emptyBuilding = new Building(elevator, 10);


        buildingWithPassengers = new Building(elevator, 10);
        int number = 1;
        int idPassenger = 1;
        for (Storey storey : buildingWithPassengers.getListStories()) {
            for (int j = 0; j < 2; j++) {
                Passenger passenger = new Passenger(idPassenger++, new Storey(1), new Storey(number));
                passenger.setTransportationState(TransportationState.COMPLETED);
                storey.getArrivalStoryContainer().add(passenger);
                listPassengers.add(passenger);
            }
            number++;
        }
    }

    @Test
    public void validationEndTransportation() throws Exception {

        boolean actual = new ValidationService().validationEndTransportation(buildingWithPassengers,listPassengers);
        assertTrue(actual);

        actual = new ValidationService().validationEndTransportation(emptyBuilding,listPassengers);
        assertFalse(actual);

    }

    @Test
    public void isEmptyDispatchContainers() throws Exception {
        boolean actual = new ValidationService().isEmptyDispatchContainers(emptyBuilding);
        assertTrue(actual);

        actual = new ValidationService().isEmptyDispatchContainers(buildingWithPassengers);
        assertTrue(actual);

    }


    @Test
    public void isPassengersInArrivalContainer() throws Exception {
        boolean actual = new ValidationService().isPassengersInArrivalContainer(buildingWithPassengers, listPassengers);
        assertTrue(actual);

        actual = new ValidationService().isPassengersInArrivalContainer(emptyBuilding, new ArrayList<>());
        assertTrue(actual);

        emptyBuilding.getListStories().get(1).getArrivalStoryContainer().add(passenger);
        actual = new ValidationService().isPassengersInArrivalContainer(emptyBuilding, listPassengers);
        assertFalse(actual);

    }

    @Test
    public void isPassengersArrivalRight() throws Exception {

        boolean actual = new ValidationService().isPassengersArrivalRight(buildingWithPassengers);
        assertTrue(actual);

        emptyBuilding.getListStories().get(1).getArrivalStoryContainer().add(passenger);
        actual = new ValidationService().isPassengersArrivalRight(emptyBuilding);
        assertFalse(actual);

    }
}

