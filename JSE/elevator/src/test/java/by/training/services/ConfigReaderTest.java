package by.training.services;

import org.junit.Test;

public class ConfigReaderTest {
    @Test(expected = RuntimeException.class)
    public void testTakeFromFileProperties() throws Exception {
        new ConfigReader().getData("config1");
    }

}