package by.training.services;

import by.training.beans.Building;
import by.training.beans.Elevator;
import by.training.beans.Passenger;
import by.training.beans.Storey;
import by.training.ifaces.PassengerCreate;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;


public class PassengerCreateRandomTest {
    @Test
    public void placePassengersOnStoreys() throws Exception {
        Building building = new Building(new Elevator(10), 10);
        int expected = 50;
        PassengerCreate passengerCreate = new PassengersCreateRandom();
        passengerCreate.placePassengersOnStoreys(building, passengerCreate.createPassengers(expected, 10));
        int actual = building.getListStories().stream().mapToInt(storey -> storey.getDispatchStoryContainer().size()).sum();
        assertEquals(expected, actual);
    }

    @Test
    public void createPassengers() throws Exception {
        ArrayList<Passenger> listPassengers = new PassengersCreateRandom().createPassengers(50, 10);

        listPassengers.forEach(passenger -> assertNotEquals(passenger.getDepartureStorey(), passenger.getDestinationStorey()));

    }
}

