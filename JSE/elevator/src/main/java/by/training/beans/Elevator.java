package by.training.beans;

import by.training.enums.ElevatorState;
import lombok.Data;
import java.util.ArrayList;

/**
 * The {@code Elevator} class represents elevator
 */
@Data
public class Elevator {
    private ArrayList<Passenger> evelvatorContainer;
    private final int elevatorCapacity;
    private ElevatorState elevatorState = ElevatorState.STOP_OUTPUT;

    /**
     * Initializes a newly created {@code Elevator} object so that it represents
     * an elevator with {@code evelvatorCapacity}
     *
     * @param evelvatorCapacity the capacity of the Elevator cabin
     */
    public Elevator(final int evelvatorCapacity) {
        this.elevatorCapacity = evelvatorCapacity;
        evelvatorContainer = new ArrayList<>();
    }
}
