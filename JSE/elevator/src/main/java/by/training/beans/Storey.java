package by.training.beans;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * The {@code Storey} class represents storey
 */
@Data
public class Storey {
    private final int numberStorey;
    private List<Passenger> dispatchStoryContainer;
    private List<Passenger> arrivalStoryContainer;


    /**
     * Initializes a newly created {@code Storey} object so that it represents
     * an storey with {@code numberStorey}, empty {@code departureStorey} and empty {@code destinationStorey}
     * @param numberStorey storey number
     */
    public Storey(final int numberStorey) {
        this.numberStorey = numberStorey;
        dispatchStoryContainer = new ArrayList<>();
        arrivalStoryContainer = new ArrayList<>();
    }
}
