package by.training.beans;

import by.training.enums.Container;
import by.training.enums.TransportationState;
import lombok.Data;

/**
 * The {@code Passenger} class represents passenger
 */
@Data
public class Passenger {
    private final int passengerId;
    private Storey departureStorey;
    private Storey destinationStorey;
    private TransportationState transportationState = TransportationState.NOT_STARTED;
    private Container container = Container.DISPATCH_CONTAINER;

    /**
     * Initializes a newly created {@code Passenger} object so that it represents
     * an Passenger with {@code passengerId}, {@code departureStorey}, {@code destinationStorey}
     *
     * @param passengerId unique passenger number
     * @param departureStorey destination storey
     * @param destinationStorey destination storey
     */
    public Passenger(final int passengerId, final Storey departureStorey, final Storey destinationStorey) {
        this.passengerId = passengerId;
        this.departureStorey = departureStorey;
        this.destinationStorey = destinationStorey;
    }
}
