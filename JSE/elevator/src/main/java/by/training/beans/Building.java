package by.training.beans;

import lombok.Data;
import java.util.LinkedList;

/**
 * The {@code Building} class represents building.
 */
@Data
public class Building {
    private Elevator elevator;
    private LinkedList<Storey> listStories;


    private void createListStories(final int storiesNumber) {
        listStories = new LinkedList<>();
        for (int i = 1; i <= storiesNumber; i++) {
            listStories.add(new Storey(i));
        }
    }
    /**
     * /**
     * Initializes a newly created {@code Building} object so that it represents
     * an building with {@code elevator} and number of floors equal {@code storiesNumber}
     *
     * @param elevator      Elevator located in this building
     * @param storiesNumber Number of floors in this building
     */
    public Building(final Elevator elevator, final int storiesNumber) {
        this.elevator = elevator;
        listStories = new LinkedList<>();
        for (int i = 1; i <= storiesNumber; i++) {
            listStories.add(new Storey(i));
        }
    }
}
