package by.training.enums;

/**
 * The state of the Elevator during transport
 */
public enum ElevatorState {
    /**
     * The elevator moves down
     */
    MOVE_DOWN,
    /**
     * The elevator moves up
     */
    MOVE_UP,
    /**
     * The elevator stands and passengers get out of it
     */
    STOP_OUTPUT,
    /**
     * The lift is stopped and passengers are entering
     */
    STOP_INPUT
}
