package by.training.enums;

/**
 * The state carriage of the passenger
 */
public enum TransportationState {
    /**
     * Creates an instance of the passenger
     */
    NOT_STARTED,
    /**
     *  Creates an instance of the Transportation Task
     */
    IN_PROGRESS,
    /**
     * Transportation Task is completed
     */
    COMPLETED
}
