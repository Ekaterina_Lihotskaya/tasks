package by.training.enums;

/**
 * The {@code ActionController} represents list of operator actions
 */
public enum  ActionController {
    /**
     * The start of the transportation process
     */
    STARTING_TRANSPORTATION,
    /**
     *  Moving elevator from floor to floor
     */
    MOVING_ELEVATOR,
    /**
     * Passenger boarding on the floor
     */
    BOADING_OF_PASSENGER,
    /**
     * Passenger deboarding on the floor
     */
    DEBOADING_OF_PASSENGER,
    /**
     * Completion of the transportation process
     */
    COMPLETION_TRANSPORTATION
}
