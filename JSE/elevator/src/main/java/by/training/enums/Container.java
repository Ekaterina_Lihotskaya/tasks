package by.training.enums;

/**
 * Containers in which the passenger may be in the process of transportation
 */
public enum Container {
    /**
     * Elevator container
     */
    ELEVATOR_CONTAINER,
    /**
     * Dispatch Container
     */
    DISPATCH_CONTAINER,
    /**
     * Arrival Container
     */
    ARRIVAL_CONTAINER
}
