package by.training.services;

import by.training.beans.Building;
import by.training.beans.Storey;
import lombok.Getter;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The class provides objects of the Condition class.
 * Each object Condition is a condition for the entrance or exit of passengers on the floor
 */
@Getter
public class ManagerCondition {
    private final ReentrantLock lock = new ReentrantLock();
    private final Condition[] conditionsInput;
    private final Condition[] conditionsOutput;
    private final Condition conditionTryOutput = lock.newCondition();
    private final Condition conditionTryInput = lock.newCondition();


    /**
     * Initializes a newly created {@code ManagerCondition} object
     * it provides the necessary conditions for the correct operation of the controller and passengers
     * @param building building
     */
    public ManagerCondition(final Building building) {
        conditionsInput = new Condition[building.getListStories().size()];
        conditionsOutput = new Condition[building.getListStories().size()];
        for (Storey storey : building.getListStories()) {
            conditionsInput[storey.getNumberStorey() - 1] = lock.newCondition();
            conditionsOutput[storey.getNumberStorey() - 1] = lock.newCondition();
        }
    }
}
