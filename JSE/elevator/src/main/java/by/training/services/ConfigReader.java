package by.training.services;

import by.training.ifaces.ReaderInitialDate;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Read source data from a file .properties
 */
public class ConfigReader implements ReaderInitialDate {

    /**
     * Get initial data
     * @param name name of file .properties
     * @return source data map
     */
    public Map<String, Integer> getData(final String name) {
        ResourceBundle resourceBundle = ResourceBundle.getBundle(name);
        int storiesNumber = Integer.parseInt(resourceBundle.getString("storiesNumber"));
        int elevatorCapacity = Integer.parseInt(resourceBundle.getString("elevatorCapacity"));
        int passengersNumber = Integer.parseInt(resourceBundle.getString("passengersNumber"));

        checkData(storiesNumber, elevatorCapacity, passengersNumber);
        Map<String, Integer> mapInitialDate = new HashMap<String, Integer>();
        mapInitialDate.put("storiesNumber", storiesNumber);
        mapInitialDate.put("elevatorCapacity", elevatorCapacity);
        mapInitialDate.put("passengersNumber", passengersNumber);
        return mapInitialDate;
    }

    private void checkData(final int storiesNumber, final int elevatorCapacity, final int passengersNumber) {
        if (storiesNumber < 2 || elevatorCapacity < 1 || passengersNumber < 1) {
            throw new RuntimeException("The file config.properties contains incorrect data");
        }
    }
}
