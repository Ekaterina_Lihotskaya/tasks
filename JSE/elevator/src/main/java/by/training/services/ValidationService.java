package by.training.services;

import by.training.beans.Building;
import by.training.beans.Passenger;
import by.training.enums.TransportationState;

import java.util.ArrayList;

/**
 * The class provides methods to verify that the transport process is completed correctly
 */
public class ValidationService {

    /**
     * Verifies the correct completion of the transportation process
     * @param building building
     * @param listPassengers list of passengers
     * @return returns true if condition fulfilled and false if not
     */
    public boolean validationEndTransportation(final Building building, final ArrayList<Passenger> listPassengers) {
        return building.getElevator().getEvelvatorContainer().isEmpty()
                && isEmptyDispatchContainers(building)
                && isPassengersInArrivalContainer(building, listPassengers)
                && isPassengersArrivalRight(building);
    }

    /**
     * Check whether the container is empty elevator
     * @param building building
     * @return returns true if condition fulfilled and false if not
     */
    public boolean isEmptyDispatchContainers(final Building building) {
       return building.getListStories().stream()
               .allMatch(storey -> (storey.getDispatchStoryContainer().isEmpty()));
    }

    /**
     * checks the containers arrival all people transportationState (must be COMPLETED)
     * and floor assignment (should match the floor number associated with the
     * corresponding arrivalStoryContainer)
     * @param building building
     * @param listPassengers list of passengers
     * @return returns true if condition fulfilled and false if not
     */
    public boolean isPassengersInArrivalContainer(final Building building, final ArrayList<Passenger> listPassengers) {
        return listPassengers.size() == building.getListStories().stream()
                .mapToInt(storey -> storey.getArrivalStoryContainer().size())
                .sum();
    }

    /**
     * checks if the total number of people in all arrivalStoryContainer matches the number of people
     * @param building building
     * @return returns true if condition fulfilled and false if not
     */
    public boolean isPassengersArrivalRight(final Building building) {
       return building.getListStories().stream().
                allMatch(storey -> (storey.getArrivalStoryContainer().stream()
                        .allMatch(passenger -> (passenger.getTransportationState() == TransportationState.COMPLETED
                                && storey.getNumberStorey() == passenger.getDestinationStorey().getNumberStorey()))));
    }
}
