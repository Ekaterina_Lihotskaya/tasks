package by.training.services;

import by.training.beans.Building;
import by.training.beans.Passenger;
import by.training.beans.Storey;
import by.training.ifaces.PassengerCreate;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Creation and random location of passengers by floors
 */
public class PassengersCreateRandom implements PassengerCreate {

    /**
     * Location of passengers by floors
     * @param building building
     * @param listPassengers list of passengers in the building
     */
    public void placePassengersOnStoreys(final Building building, final List<Passenger> listPassengers) {
        for (Passenger passenger : listPassengers) {
            int departureStorey = passenger.getDepartureStorey().getNumberStorey();
            Storey storey = building.getListStories().get(departureStorey - 1);
            storey.getDispatchStoryContainer().add(passenger);
        }
    }

    /**
     * Creation of the required number of passengers
     * @param passengersNumber  number of passengers for transportation
     * @param storiesNumber number of floors in the building
     * @return list of passengers
     */
    public ArrayList<Passenger> createPassengers(final int passengersNumber, final int storiesNumber) {
        Random rand = new Random();
        ArrayList<Passenger> listPassengers = new ArrayList<Passenger>();
        for (int i = 1; i <= passengersNumber; i++) {
            int departureStorey = rand.nextInt(storiesNumber);
            int destinationStorey;
            do {
                destinationStorey = rand.nextInt(storiesNumber);
            }
            while (departureStorey == destinationStorey);
            Passenger passenger = new Passenger(i, new Storey(departureStorey + 1), new Storey(destinationStorey + 1));
            listPassengers.add(passenger);
        }
        return listPassengers;
    }
}
