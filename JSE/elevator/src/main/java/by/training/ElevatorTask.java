package by.training;

import by.training.beans.Building;
import by.training.beans.Elevator;
import by.training.beans.Passenger;
import by.training.ifaces.PassengerCreate;
import by.training.ifaces.TransportationControllerAbstract;
import by.training.imp.ConsistentController;
import by.training.imp.TransportationTask;
import by.training.services.PassengersCreateRandom;
import by.training.services.ConfigReader;
import by.training.services.ValidationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ElevatorTask {
    private static final Logger rootLogger = LogManager.getRootLogger();

    /**
     * Start application
     *
     * @param args initial args
     */
    public static void main(final String[] args) {

        Map<String, Integer> mapInitialDate = new ConfigReader().getData("config");

        Elevator elevator = new Elevator(mapInitialDate.get("elevatorCapacity"));
        Building building = new Building(elevator, mapInitialDate.get("storiesNumber"));

        PassengerCreate passengerCreate = new PassengersCreateRandom();
        ArrayList<Passenger> listPassengers = passengerCreate.
                createPassengers(mapInitialDate.get("passengersNumber"), mapInitialDate.get("storiesNumber"));
        passengerCreate.placePassengersOnStoreys(building, listPassengers);


        CountDownLatch latch = new CountDownLatch(mapInitialDate.get("passengersNumber"));
        TransportationControllerAbstract controller = new ConsistentController(building, latch);
        ExecutorService service = Executors.newFixedThreadPool(mapInitialDate.get("passengersNumber"));
        for (Passenger passenger : listPassengers) {
            service.submit((new TransportationTask(passenger, controller, elevator, latch)));
        }

        try {
            controller.moveElevator();
            ValidationService validationService = new ValidationService();
            if (validationService.validationEndTransportation(building, listPassengers)) {
                rootLogger.info("The transportation process is completed correctly");
            } else {
                rootLogger.info("The transportation process is completed incorrectly");
            }
        } catch (InterruptedException e) {
            rootLogger.info("The transportation process is completed incorrectly");
        } finally {
            service.shutdown();
        }
    }
}
