package by.training.imp;

import by.training.beans.Elevator;
import by.training.beans.Passenger;
import by.training.enums.ElevatorState;
import by.training.ifaces.TransportationControllerAbstract;
import by.training.ifaces.TransportationTaskAbstract;

import java.util.concurrent.CountDownLatch;

/**
 * The {@code TransportationTask} class represents Thread for instance of passenger
 */

public class TransportationTask extends TransportationTaskAbstract {

    /**
     * Checks if there is a condition for the exit of passengers
     *
     * @return true if the passenger can get out and false if he should not get out
     */
    public boolean isConditionExit() {
        return elevator.getElevatorState() == ElevatorState.STOP_OUTPUT;
    }

    /**
     * Checks if there is a condition for passengers to enter
     *
     * @return true if the passenger can enter and false if he should not enter
     */
    public boolean isConditionEnter() {
        ElevatorState moveStatePassenger = ElevatorState.MOVE_DOWN;
        if (passenger.getDestinationStorey().getNumberStorey() - passenger.getDepartureStorey().getNumberStorey() > 0) {
            moveStatePassenger = ElevatorState.MOVE_UP;
        }
        return elevator.getElevatorState() == ElevatorState.STOP_INPUT
                && controller.getElevatorState() == moveStatePassenger;
    }

    /**
     * Initializes a newly created {@code TransportationTask} object
     * @param passenger passenger
     * @param controller controller
     * @param elevator elevator
     * @param latch the controller will start working when all TransportationTask are created and the counter is 0
     *              each TransportationTask reduces the counter when sending a request to enter the Elevator
     */
    public TransportationTask(final Passenger passenger, final TransportationControllerAbstract controller,
                              final Elevator elevator, final CountDownLatch latch) {
        super(passenger, controller, elevator, latch);
    }
}