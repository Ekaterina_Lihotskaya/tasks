package by.training.imp;

import by.training.beans.Building;
import by.training.beans.Elevator;
import by.training.beans.Storey;
import by.training.enums.ActionController;
import by.training.enums.ElevatorState;
import by.training.ifaces.TransportationControllerAbstract;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.concurrent.CountDownLatch;

/**
 * The {@code ConsistentController} class represents a controller that controls
 * the elevator in series, first up and then down
 */
@Data@EqualsAndHashCode(callSuper = false)
public class ConsistentController extends TransportationControllerAbstract {
    private static final Logger rootLogger = LogManager.getRootLogger();

    /**
     * Describes the process of movement of the Elevator
     * @throws InterruptedException the exception is passed to the main method
     */
    public void moveElevator() throws InterruptedException {
        latch.await();
        rootLogger.info(ActionController.STARTING_TRANSPORTATION);
        Elevator elevator = building.getElevator();
        LinkedList<Storey> listStories = building.getListStories();

        while (!isEndMove) {
            elevatorState = ElevatorState.MOVE_UP;
            moveUP(listStories, elevator);
            elevatorState = ElevatorState.MOVE_DOWN;
            moveDOWN(listStories, elevator);
        }
        rootLogger.info(ActionController.COMPLETION_TRANSPORTATION);
    }

    /**
     * Initializes a newly created {@code ConsistentController} object
     *
     * @param building building
     * @param latch the controller will start working when all TransportationTask are created and the counter is 0
     */
    public ConsistentController(final Building building, final CountDownLatch latch) {
        super(building, latch);
    }
}
