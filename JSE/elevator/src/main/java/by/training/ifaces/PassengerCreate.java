package by.training.ifaces;

import by.training.beans.Building;
import by.training.beans.Passenger;
import java.util.ArrayList;
import java.util.List;

/**
 * Contract of creation and location of passengers by floors
 */
public interface PassengerCreate {
    /**
     * Location of passengers by floors
     * @param building building
     * @param listPassengers list of passengers in the building
     */
    void placePassengersOnStoreys(Building building, List<Passenger> listPassengers);

    /**
     * Creation of the required number of passengers
     * @param passengersNumber  number of passengers for transportation
     * @param storiesNumber number of floors in the building
     * @return list of passengers
     */
    ArrayList<Passenger> createPassengers(int passengersNumber, int storiesNumber);
}
