package by.training.ifaces;

import by.training.beans.Elevator;
import by.training.beans.Passenger;
import by.training.enums.TransportationState;
import by.training.services.ManagerCondition;

import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

/**
 * The {@code TransportationTaskAbstract} class represents Thread for instance of passenger
 */

public abstract class TransportationTaskAbstract implements Callable<String> {
    protected Passenger passenger;
    protected TransportationControllerAbstract controller;
    protected Elevator elevator;
    private CountDownLatch latch;

    /**
     * Starts the execution of the TransportationTask
     * @return the name of the current Thread
     * @throws InterruptedException the exception is passed to the main method
     */
    public String call() throws InterruptedException {
        passenger.setTransportationState(TransportationState.IN_PROGRESS);
        movePassenger();
        passenger.setTransportationState(TransportationState.COMPLETED);
        return Thread.currentThread().getName();
    }

    /**
     * Starts the process of the movement of passengers
     * which is composed of the request input and request output
     * @throws InterruptedException the exception is passed to the main method
     */
    public void movePassenger() throws InterruptedException {
        System.out.println("Passenger " + passenger.getPassengerId() + " "
                + passenger.getDepartureStorey().getNumberStorey() + "---"
                + passenger.getDestinationStorey().getNumberStorey());
        requestEnter();
        requestExit();
    }

    /**
     * Passenger request for exit
     * The passenger waits until the controller tells him that the elevator has arrived on his floor and he can exit
     * @throws InterruptedException the exception is passed to the main method
     */
    public void requestExit() throws InterruptedException {
        ManagerCondition managerCondition = controller.getManagerCondition();
        while (!isConditionExit()) {
            managerCondition.getConditionsOutput()[passenger.getDestinationStorey().getNumberStorey() - 1].await();
        }
        controller.setCurrentPassenger(passenger);
        managerCondition.getConditionTryOutput().signal();
        managerCondition.getLock().unlock();
    }

    /**
     * Passenger request for entry
     * The passenger waits until the controller tells him that the elevator has arrived on his floor and he can enter
     * @throws InterruptedException the exception is passed to the main method
     */
    public void requestEnter() throws InterruptedException {
        ManagerCondition managerCondition = controller.getManagerCondition();
        managerCondition.getLock().lock();

        while (!isConditionEnter()) {
            managerCondition.getConditionTryInput().signal();
            latch.countDown();
            managerCondition.getConditionsInput()[passenger.getDepartureStorey().getNumberStorey() - 1].await();
        }
        controller.setCurrentPassenger(passenger);
        managerCondition.getConditionTryInput().signal();
    }

    /**
     * Checks if there is a condition for passengers to enter
     *
     * @return true if the passenger can enter and false if he should not enter
     */
    public abstract boolean isConditionEnter();

    /**
     * Checks if there is a condition for the exit of passengers
     *
     * @return true if the passenger can get out and false if he should not get out
     */
    public abstract boolean isConditionExit();

    /**
     * Initializes a newly created {@code TransportationTask} object
     * @param passenger passenger
     * @param controller controller
     * @param elevator elevator
     * @param latch the controller will start working when all TransportationTask are created and the counter is 0
     *              each TransportationTask reduces the counter when sending a request to enter the Elevator
     */
    public TransportationTaskAbstract(final Passenger passenger, final TransportationControllerAbstract controller,
                                      final Elevator elevator, final CountDownLatch latch) {
        this.passenger = passenger;
        this.controller = controller;
        this.elevator = elevator;
        this.latch = latch;
    }
}

