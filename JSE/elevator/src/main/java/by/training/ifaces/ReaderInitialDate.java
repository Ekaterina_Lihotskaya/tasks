package by.training.ifaces;

import java.util.Map;

/**
 * The contract for the initial data
 */
public interface ReaderInitialDate {

    /**
     * Get initial data
     * @param name data source name
     * @return source data map
     */
    Map<String, Integer> getData(String name);
}
