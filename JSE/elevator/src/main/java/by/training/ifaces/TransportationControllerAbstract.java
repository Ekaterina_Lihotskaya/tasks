package by.training.ifaces;

import by.training.beans.Building;
import by.training.beans.Elevator;
import by.training.beans.Passenger;
import by.training.beans.Storey;
import by.training.enums.ActionController;
import by.training.enums.Container;
import by.training.enums.ElevatorState;
import by.training.services.ManagerCondition;
import by.training.services.ValidationService;
import lombok.Data;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Condition;

/**
 * The {@code TransportationControllerAbstract} class represents a controller
 * who manages the Elevator work
 */
@Data

public abstract class TransportationControllerAbstract {
    private static final Logger rootLogger = LogManager.getRootLogger();
    protected Building building;
    protected ManagerCondition managerCondition;
    private Passenger currentPassenger;
    private int currentStorey;
    protected ElevatorState elevatorState = ElevatorState.MOVE_UP;
    protected CountDownLatch latch;

    protected boolean isEndMove = false;

    /**
     * Describes the process of movement of the Elevator
     * @throws InterruptedException the exception is passed to the main method
     */
    public abstract void moveElevator() throws InterruptedException;

    /**
     * The movement of the Elevator up
     * @param listStories list of stories
     * @param elevator elevator
     * @throws InterruptedException the exception is passed to the main method
     */
    public void moveUP(final LinkedList<Storey> listStories, final Elevator elevator) throws InterruptedException {
        ListIterator<Storey> litForward = listStories.listIterator();
        while (litForward.hasNext() && !isEndMove) {
            Storey storey = litForward.next();
            if (litForward.hasNext()) {
                exitEnterPassengers(elevator, storey);
                rootLogger.info(ActionController.MOVING_ELEVATOR + " from story-"
                        + currentStorey + " to story-" + (currentStorey + 1));
            }
        }
    }

    /**
     *
     * The movement of the Elevator down
     * @param listStories list of stories
     * @param elevator elevator
     * @throws InterruptedException the exception is passed to the main method
     */
    public void moveDOWN(final LinkedList<Storey> listStories, final Elevator elevator) throws InterruptedException {
        ListIterator<Storey> litBackward = listStories.listIterator(listStories.size());
        while (litBackward.hasPrevious() && !isEndMove) {
            Storey storey = litBackward.previous();
            if (litBackward.hasPrevious()) {
                exitEnterPassengers(elevator, storey);
                rootLogger.info(ActionController.MOVING_ELEVATOR + " from story-"
                        + currentStorey + " to story-" + (currentStorey - 1));
            }
        }
    }

    /**
     * Exit/enter passengers
     *
     * @param elevator elevator
     * @param storey current storey
     * @throws InterruptedException the exception is passed to the main method
     */
    public void exitEnterPassengers(final Elevator elevator, final Storey storey) throws InterruptedException {
        currentStorey = storey.getNumberStorey();
        System.out.println("Come to the " + currentStorey);
        if (elevator.getEvelvatorContainer().size() != 0) {
            exitPassengers(elevator, storey);
        }
        enterPassengers(elevator, storey);
        isEndMove = elevator.getEvelvatorContainer().isEmpty()
                && new ValidationService().isEmptyDispatchContainers(building);
    }

    /**
     * The process of exiting passengers.
     * The controller stops the Elevator and tells passengers that the Elevator
     * has arrived on the desired floor and they can go out
     *
     * @param elevator elevator
     * @param storey current storey
     * @throws InterruptedException the exception is passed to the main method
     */
    public void exitPassengers(final Elevator elevator, final Storey storey) throws InterruptedException {
        Condition conditionOutput = managerCondition.getConditionsOutput()[storey.getNumberStorey() - 1];

        long countTaskForOutput = elevator.getEvelvatorContainer().stream().
                filter(passenger -> passenger.getDestinationStorey().getNumberStorey() == (storey.getNumberStorey()))
                .count();

        managerCondition.getLock().lock();
        elevator.setElevatorState(ElevatorState.STOP_OUTPUT);
        while (countTaskForOutput != 0) {
            conditionOutput.signal();
            managerCondition.getConditionTryOutput().await();
            countTaskForOutput--;
            deboadingOfPassengers(storey);
        }
        managerCondition.getLock().unlock();
    }

    /**
     * The controller moves a person from evelvatorContainer in arrivalStoryContainer
     * @param storey storey
     */
    public void deboadingOfPassengers(final Storey storey) {
        storey.getArrivalStoryContainer().add(currentPassenger);
        building.getElevator().getEvelvatorContainer().remove(currentPassenger);
        currentPassenger.setContainer(Container.ARRIVAL_CONTAINER);
        rootLogger.info("" + ActionController.DEBOADING_OF_PASSENGER
                + currentPassenger.getPassengerId() + " on story-" + currentStorey);
        currentPassenger = null;
    }

/**
 * Passengers can enter the Elevator.
 * The controller tells passengers that the Elevator has arrived on the desired floor and they can enter
 *
 * @param elevator elevator
 * @param storey current storey
 * @throws InterruptedException the exception is passed to the main method
 */
    public void enterPassengers(final Elevator elevator, final Storey storey) throws InterruptedException {
        Condition conditionInput = managerCondition.getConditionsInput()[storey.getNumberStorey() - 1];
        int countTaskForInput = building.getListStories().get(storey.getNumberStorey() - 1)
                .getDispatchStoryContainer().size();

        managerCondition.getLock().lock();
        elevator.setElevatorState(ElevatorState.STOP_INPUT);
        while ((countTaskForInput != 0) && (elevator.getEvelvatorContainer().size()
                != elevator.getElevatorCapacity())) {
            conditionInput.signal();
            managerCondition.getConditionTryInput().await();
            if (currentPassenger != null) {
                boadingOfPassengers(storey);
            }
            countTaskForInput--;
        }
        managerCondition.getLock().unlock();
    }

    /**
     * The controller moves a person from dispatchStoryContainer in evelvatorContainer
     * @param storey storey
     */
    public void boadingOfPassengers(final Storey storey) {
        storey.getDispatchStoryContainer().remove(currentPassenger);
        building.getElevator().getEvelvatorContainer().add(currentPassenger);
        currentPassenger.setContainer(Container.ELEVATOR_CONTAINER);
        rootLogger.info("" + ActionController.BOADING_OF_PASSENGER + currentPassenger.getPassengerId()
                + " on story-" + currentStorey);
        currentPassenger = null;
    }

    /**
     * Initializes a newly created {@code TransportationControllerAbstract} object
     *
     * @param building building
     * @param latch the controller will start working when all TransportationTask are created and the counter is 0
     */
    public TransportationControllerAbstract(final Building building, final CountDownLatch latch) {
        this.building = building;
        managerCondition = new ManagerCondition(building);
        this.latch = latch;
    }
}
