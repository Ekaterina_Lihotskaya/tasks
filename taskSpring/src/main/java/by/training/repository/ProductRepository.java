package by.training.repository;

import by.training.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {

    Page<Product> findByName(String name, Pageable pageable);

    Page<Product> findByPricesPriceAndPricesCurrency(Long price, String currency, Pageable pageable);

    Page<Product> findByCategoriesId(Long id, Pageable pageable);
}
