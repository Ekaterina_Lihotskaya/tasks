package by.training.repository;

import by.training.model.Price;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface PriceRepository extends PagingAndSortingRepository<Price, Long> {

    Page<Price> findByCurrency(String currency, Pageable pageable);

    Page<Price> findByCurrencyAndPriceBetween(String currency, Pageable pageable, Long start, Long end);

    Page<Price> findByProductsId(Long idProduct, Pageable pageable);
}
