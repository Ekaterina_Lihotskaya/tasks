package by.training.repository;

import by.training.model.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface CategoryRepository extends PagingAndSortingRepository<Category, Long> {

    Page<Category> findByName(String name, Pageable pageable);
}
