package by.training;

import by.training.model.Category;
import by.training.model.Price;
import by.training.model.Product;
import by.training.repository.CategoryRepository;
import by.training.repository.PriceRepository;
import by.training.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

@Component
public class DataInit implements ApplicationRunner {

    private ProductRepository productRepository;
    private CategoryRepository categoryRepository;
    private PriceRepository priceRepository;

    @Autowired
    public DataInit(ProductRepository productRepository, CategoryRepository categoryRepository, PriceRepository priceRepository) {
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
        this.priceRepository = priceRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        long countProduct = 120;
        long countCategory = 100;
        long countPrices = 80;

        List<Category> categories = createCategories(countCategory);
        List<Price> prices = createPrices(countPrices);
        List<Product> products = createProducts(countProduct, categories, prices);
    }

    private List<Product> createProducts(long countProduct, List<Category> categories, List<Price> prices) {
     /*   List<Product> products = new ArrayList<Product>();

        for (int i = 1; i < countProduct + 1; i++) {
            Product product = new Product("Product " + i, "This is product № " + i);
            products.add(product);
            productRepository.save(product);


            product.setCategories(Arrays.asList(categories.get((int) (ThreadLocalRandom.current().nextLong(1, categories.size()))),
                    categories.get((int) (ThreadLocalRandom.current().nextLong(1, categories.size())))));

            product.setPrices(Arrays.asList(prices.get((int) (ThreadLocalRandom.current().nextLong(1, prices.size()))),
                    prices.get((int) (ThreadLocalRandom.current().nextLong(1, prices.size())))));

            productRepository.save(product);
        }
        return products;*/

        List<Product> products = new ArrayList<Product>();

        for (int i = 1; i < countProduct + 1; i++) {
            products.add((new Product("Product " + i, "This is product № " + i)));
        }
        productRepository.saveAll(products);
        products.forEach(x -> {
            x.setCategories(Arrays.asList(categories.get((int) (ThreadLocalRandom.current().nextLong(1, categories.size()))),
                    categories.get((int) (ThreadLocalRandom.current().nextLong(1, categories.size())))));
            x.setPrices(Arrays.asList(prices.get((int) (ThreadLocalRandom.current().nextLong(1, prices.size()))),
                    prices.get((int) (ThreadLocalRandom.current().nextLong(1, prices.size())))));
        });

        productRepository.saveAll(products);
        return products;
    }


    private List<Category> createCategories(long countCategory) {
        List<Category> categories = new ArrayList<>();
        categories.add(categoryRepository.save(new Category("main")));

        for (int i = 1; i < countCategory; i++) {
            categories.add(categoryRepository.save(new Category(("Category " + i))));
        }

        for (int i = 0; i < categories.size(); i++) {
            Optional<Category> category = categoryRepository.findById(ThreadLocalRandom.current().nextLong(1, categories.size() + 1));
            if (category.isPresent()) {
                categories.get(i).setSuperCategory(categoryRepository.findById(ThreadLocalRandom.current().nextLong(1, countCategory + 1)).get());
                categoryRepository.save(categories.get(i));
            }

        }


       /* categories.forEach(x -> x.setSuperCategory(categoryRepository.findById(ThreadLocalRandom.current().nextLong(1, countCategory + 1)).get()));
        categoryRepository.saveAll(categories);*/
        return categories;
    }

    private List<Price> createPrices(long countPrices) {
        List<Price> prices = new ArrayList<>();
        for (int i = 1; i < countPrices + 1; i++) {
            String currency;
            if (i % 2 == 0) {
                currency = "BY";
            } else {
                currency = "USD";
            }
            prices.add(priceRepository.save(new Price(ThreadLocalRandom.current().nextLong(0, 150000), currency)));
        }
        // priceRepository.saveAll(prices);
        return prices;
    }
}
