package by.training;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskspringApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaskspringApplication.class, args);


	}

}
