package by.training.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ProductDto {
    private Long id;
    private String name;
    private String description;
    private List<Long> pricesId = new ArrayList<>();
    private List<Long> categoriesId = new ArrayList<>();
}
