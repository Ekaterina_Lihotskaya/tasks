package by.training.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CategoryDto {
    private Long id;
    private String name;
    private List<Long> productsId = new ArrayList<>();
    private Long idParentCategory;

}
