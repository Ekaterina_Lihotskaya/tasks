package by.training.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class PriceDto {
    private Long id;
    private Long price;
    private String currency;
    private List<Long> productsId = new ArrayList<>();
}
