package by.training.converter;

import by.training.dto.CategoryDto;
import by.training.model.Category;
import by.training.model.Product;
import by.training.repository.CategoryRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Data
@Component
public class CategoryConverter implements Converter<Category, CategoryDto> {
    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryConverter(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public CategoryDto convertModelToDto(Category category) {
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setId(category.getId());
        categoryDto.setName(category.getName());
        categoryDto.setIdParentCategory(category.getSuperCategory() != null ? category.getSuperCategory().getId() : null);
        categoryDto.setProductsId(category.getProducts().stream().map(Product::getId).collect(Collectors.toList()));
        return categoryDto;
    }

    @Override
    public Category convertDtoToModel(CategoryDto categoryDto) {
        Category category = new Category();
        category.setId(categoryDto.getId());
        category.setName(categoryDto.getName());
        category.setSuperCategory(categoryDto.getIdParentCategory() != null ? categoryRepository.findById(categoryDto.getIdParentCategory()).orElse(null) : null);
        return category;
    }
}