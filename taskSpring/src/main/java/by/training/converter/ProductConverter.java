package by.training.converter;

import by.training.dto.ProductDto;
import by.training.model.Category;
import by.training.model.Price;
import by.training.model.Product;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class ProductConverter implements Converter<Product, ProductDto> {

    @Override
    public ProductDto convertModelToDto(Product product) {
        ProductDto productDto = new ProductDto();
        productDto.setId(product.getId());
        productDto.setName(product.getName());
        productDto.setDescription(product.getDescription());
        productDto.setCategoriesId(product.getCategories().stream().map(Category::getId).collect(Collectors.toList()));
        productDto.setPricesId(product.getPrices().stream().map(Price::getId).collect(Collectors.toList()));
        return productDto;
    }

    @Override
    public Product convertDtoToModel(ProductDto productDto) {
        Product product = new Product();
        product.setId(productDto.getId());
        product.setName(productDto.getName());
        product.setDescription(productDto.getDescription());
        product.setCategories(productDto.getCategoriesId().stream().map(id -> {
            Category category = new Category();
            category.setId(id);
            return category;
        }).collect(Collectors.toList()));

        product.setPrices(productDto.getPricesId().stream().map(id -> {
            Price price = new Price();
            price.setId(id);
            return price;
        }).collect(Collectors.toList()));
        return product;
    }
}
