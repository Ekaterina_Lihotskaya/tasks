package by.training.converter;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ConverterUtils {

    public <T, R> List<R> convertAll(Page<T> page, Converter<T, R> converter) {
        List<T> list = new ArrayList<>();
        page.forEach(list::add);
        return list.stream()
                .map(converter::convertModelToDto)
                .collect(Collectors.toList());
    }

}
