package by.training.converter;

import org.springframework.stereotype.Component;

@Component
public interface Converter<T, R> {
    R convertModelToDto(T t);

    T convertDtoToModel(R r);
}
