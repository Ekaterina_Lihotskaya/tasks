package by.training.converter;

import by.training.dto.PriceDto;
import by.training.model.Price;
import by.training.model.Product;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class PriceConverter implements Converter<Price, PriceDto> {

    @Override
    public PriceDto convertModelToDto(Price price) {
        PriceDto priceDto = new PriceDto();
        priceDto.setId(price.getId());
        priceDto.setPrice(price.getPrice());
        priceDto.setCurrency(price.getCurrency());
        priceDto.setProductsId(price.getProducts().stream().map(Product::getId).collect(Collectors.toList()));
        return priceDto;
    }

    @Override
    public Price convertDtoToModel(PriceDto priceDto) {
        Price price = new Price();
        price.setId(priceDto.getId());
        price.setPrice(priceDto.getPrice());
        price.setCurrency(priceDto.getCurrency());
        return price;
    }
}