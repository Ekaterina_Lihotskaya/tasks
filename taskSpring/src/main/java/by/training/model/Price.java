package by.training.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
public class Price {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String currency;


    @Column(nullable = false)
    private Long price;


    @ManyToMany(mappedBy = "prices")
    private List<Product> products = new ArrayList<>();


    public Price(Long price, String currency) {
        this.price = price;
        this.currency = currency;
    }
}
