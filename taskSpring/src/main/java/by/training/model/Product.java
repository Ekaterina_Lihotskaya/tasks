package by.training.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;


    @ManyToMany
    @JoinTable(name = "product_category", joinColumns = @JoinColumn(name = "product_id"),
            inverseJoinColumns = @JoinColumn(name = "category_id"))
    private List<Category> categories = new ArrayList<>();


    @ManyToMany
    @JoinTable(name = "product_price", joinColumns = @JoinColumn(name = "product_id"),
            inverseJoinColumns = @JoinColumn(name = "price_id"))
    private List<Price> prices = new ArrayList<>();


    public Product(String name, String description, List<Price> prices, List<Category> categories) {
        this.name = name;
        this.description = description;
        this.prices = prices;
        this.categories = categories;
    }


    public Product(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
