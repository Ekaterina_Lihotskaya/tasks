package by.training.web.controller;

import by.training.dto.ProductDto;
import by.training.facade.ProductFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/product")
public class ProductRestController {

    private final ProductFacade productFacade;

    @Autowired
    public ProductRestController(ProductFacade productFacade) {
        this.productFacade = productFacade;
    }


    @PostMapping
    public ResponseEntity<ProductDto> createProduct(@RequestBody ProductDto productDto) {
        if (productDto == null) {
            return ResponseEntity.badRequest().build();
        }
        ProductDto productDtoReturn = productFacade.create(productDto);

        if (productDtoReturn == null){
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(productDtoReturn);
    }


    @PutMapping
    public ResponseEntity<ProductDto> updateProduct(@RequestBody ProductDto productDto) {
        if (productDto == null) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(productFacade.update(productDto));
    }



    @DeleteMapping(value = "/{id}")
    public ResponseEntity<ProductDto> deleteProduct(@PathVariable("id") Long productId) {
        if (productFacade.findById(productId) == null) {
            return ResponseEntity.notFound().build();
        }
        productFacade.delete(productId);
        return ResponseEntity.ok().build();
    }


    @GetMapping
    public ResponseEntity<List<ProductDto>> getAllProduct(@PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        List<ProductDto> products = productFacade.findAll(pageable);
        if (products.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(products);
    }


    @GetMapping(value = "/search/{id}")
    public ResponseEntity<ProductDto> getProductById(@PathVariable("id") Long productId) {
        ProductDto productDto = productFacade.findById(productId);
        if (productDto == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(productDto);
    }

    @GetMapping(value = "/search", params = "name")
    public ResponseEntity<List<ProductDto>> getProductByName(@RequestParam(value = "name") String name,
                                                             @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        if (name == null) {
            return ResponseEntity.badRequest().build();
        }
        List<ProductDto> products = productFacade.findByName(name, pageable);
        if (products.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(products);
    }


    @GetMapping(value = "/search", params = {"price", "currency"})
    public ResponseEntity<List<ProductDto>> getProductByPrice(@RequestParam(value = "price") Long price,
                                                              @RequestParam(value = "currency") String currency,
                                                              @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        if (price == null || currency == null) {
            return ResponseEntity.badRequest().build();
        }
        List<ProductDto> products = productFacade.findByPrice(price, currency, pageable);
        if (products.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(products);
    }


    @GetMapping(value = "/search", params = "idCategory")
    public ResponseEntity<List<ProductDto>> getProductByCategory(@RequestParam(value = "idCategory") Long idCategory,
                                                                 @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        if (idCategory == null) {
            return ResponseEntity.badRequest().build();
        }
        List<ProductDto> products = productFacade.findByCategory(idCategory, pageable);
        if (products.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(products);
    }
}

