package by.training.web.controller;

import by.training.dto.CategoryDto;
import by.training.facade.CategoryFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryRestController {

    private final CategoryFacade categoryFacade;

    @Autowired
    public CategoryRestController(CategoryFacade categoryFacade) {
        this.categoryFacade = categoryFacade;
    }


    @PostMapping
    public ResponseEntity<CategoryDto> createCategory(@RequestBody CategoryDto categoryDto) {
        if (categoryDto == null) {
            return ResponseEntity.badRequest().build();
        }
        CategoryDto categoryDtoReturn = (categoryFacade.create(categoryDto));

        if (categoryDtoReturn == null){
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(categoryDtoReturn);
    }

    @PutMapping
    public ResponseEntity<CategoryDto> updateCategory(@RequestBody CategoryDto categoryDto) {
        if (categoryDto == null) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(categoryFacade.update(categoryDto));
    }


    @DeleteMapping(value = "/{id}")
    public ResponseEntity<CategoryDto> deleteCategory(@PathVariable("id") Long categoryId) {
        if (categoryFacade.findById(categoryId) == null) {
            return ResponseEntity.notFound().build();
        }

        categoryFacade.delete(categoryId);
        return ResponseEntity.ok().build();
    }


    @GetMapping
    public ResponseEntity<List<CategoryDto>> getAllCategory(@PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        List<CategoryDto> categories = categoryFacade.findAll(pageable);
        if (categories.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(categories);
    }


    @GetMapping(value = "/search/{id}")
    public ResponseEntity<CategoryDto> getCategoryByCode(@PathVariable("id") Long idCategory) {
        CategoryDto categoryDto = categoryFacade.findById(idCategory);
        if (categoryDto == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(categoryDto);

    }
    @GetMapping(value = "/search", params = "name")
    public ResponseEntity<List<CategoryDto>> getCategoryByName(@RequestParam(value = "name") String name,
                                                             @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        if (name == null) {
            return ResponseEntity.badRequest().build();
        }
        List<CategoryDto> categories = categoryFacade.findByName(name, pageable);
        if (categories.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(categories);
    }

}
