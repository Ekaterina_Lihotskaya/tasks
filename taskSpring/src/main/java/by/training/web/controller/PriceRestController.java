package by.training.web.controller;

import by.training.dto.PriceDto;
import by.training.dto.ProductDto;
import by.training.facade.PriceFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/price")
public class PriceRestController {

    private final PriceFacade priceFacade;

    @Autowired
    public PriceRestController(PriceFacade priceFacade) {
        this.priceFacade = priceFacade;
    }

    @PostMapping
    public ResponseEntity<PriceDto> createPrice(@RequestBody PriceDto priceDto) {
        if (priceDto == null) {
            return ResponseEntity.badRequest().build();
        }
        PriceDto priceDtoReturn = priceFacade.create(priceDto);

        if (priceDtoReturn == null){
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(priceDtoReturn);
    }


    @PutMapping
    public ResponseEntity<PriceDto> updatePrice(@RequestBody PriceDto priceDto) {
        if (priceDto == null ) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(priceFacade.update(priceDto));
    }


    @DeleteMapping(value = "/{id}")
    public ResponseEntity<PriceDto> deletePrice(@PathVariable("id") Long priceId) {
        if (priceFacade.findById(priceId) == null) {
            return ResponseEntity.notFound().build();
        }
        priceFacade.delete(priceId);
        return ResponseEntity.ok().build();
    }


    @GetMapping
    public ResponseEntity<List<PriceDto>> getAllPrice(@PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        List<PriceDto> prices = priceFacade.findAll(pageable);
        if (prices.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(prices);
    }

    @GetMapping(value = "/search/{id}")
    public ResponseEntity<PriceDto> getPriceById(@PathVariable("id") Long priceId) {
        PriceDto priceDto = priceFacade.findById(priceId);
        if (priceDto == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(priceDto);

    }


    @GetMapping(value = "/search", params = "currency")
    public ResponseEntity<List<PriceDto>> getPriceByCurrency(@RequestParam(value = "currency") String currency,
                                                             @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        if (currency == null) {
            return ResponseEntity.badRequest().build();
        }
        List<PriceDto> prices = priceFacade.findByCurrency(currency, pageable);
        if (prices.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(prices);
    }


    @GetMapping(value = "/search", params = {"currency", "start", "end"})
    public ResponseEntity<List<PriceDto>> getPriceByRange(@RequestParam(value = "start") Long start,
                                                        @RequestParam(value = "currency") String currency,
                                                        @RequestParam(value = "end") Long end,
                                                        @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        if (currency == null || start == null || end == null) {
            return ResponseEntity.badRequest().build();
        }
        List<PriceDto> prices = priceFacade.findPriceByRange(currency, pageable, start, end);
        if (prices.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(prices);
    }

    @GetMapping(value = "/search", params = "idProduct")
    public ResponseEntity<List<PriceDto>> getPriceByProduct(@RequestParam(value = "idProduct") Long idProduct,
                                                        @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        if (idProduct == null) {
            return ResponseEntity.badRequest().build();
        }
        List<PriceDto> prices = priceFacade.findByProduct(idProduct, pageable);
        if (prices.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(prices);
    }


}
