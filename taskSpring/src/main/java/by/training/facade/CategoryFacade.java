package by.training.facade;


import by.training.dto.CategoryDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CategoryFacade {

    CategoryDto create(CategoryDto categoryDto);

    CategoryDto update(CategoryDto categoryDto);

    void delete(Long id);

    List<CategoryDto> findAll(Pageable pageable);

    CategoryDto findById(Long id);

    List<CategoryDto> findByName(String name, Pageable pageable);

}
