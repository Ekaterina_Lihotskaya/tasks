package by.training.facade;

import by.training.converter.Converter;
import by.training.converter.ConverterUtils;
import by.training.dto.ProductDto;
import by.training.model.Product;
import by.training.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProductFacadeImpl implements ProductFacade {

    private final ProductRepository productRepository;
    private final Converter<Product, ProductDto> productConverter;
    private final ConverterUtils converterUtils;

    @Autowired
    public ProductFacadeImpl(ProductRepository productRepository, Converter<Product, ProductDto> productConverter, ConverterUtils converterUtils) {
        this.productRepository = productRepository;
        this.productConverter = productConverter;
        this.converterUtils = converterUtils;
    }

    @Override
    public ProductDto create(final ProductDto productDto) {
        if (productRepository.findById(productDto.getId()).isPresent()){
            Product model = productConverter.convertDtoToModel(productDto);
            Product savedModel = productRepository.save(model);
            return productConverter.convertModelToDto(savedModel);

        }
        return  null;
    }

    @Override
    public ProductDto update(ProductDto productDto) {
        Product model = productConverter.convertDtoToModel(productDto);
        model.setId(productDto.getId());
        Product savedModel = productRepository.save(model);
        return productConverter.convertModelToDto(savedModel);
    }

    @Override
    public void delete(Long id) {
        Product model = new Product();
        model.setId(id);
        productRepository.delete(model);
    }

    @Override
    public List<ProductDto> findAll(Pageable pageable) {
        return converterUtils.convertAll(productRepository.findAll(pageable), productConverter);
    }

    @Override
    public ProductDto findById(Long id) {
        if (productRepository.findById(id).isPresent()) {
            return productConverter.convertModelToDto(productRepository.findById(id).get());
        } else
            return null;
    }

    @Override
    public List<ProductDto> findByName(String name, Pageable pageable) {
        return converterUtils.convertAll(productRepository.findByName(name, pageable), productConverter);
    }

    @Override
    public List<ProductDto> findByPrice(Long price, String currency, Pageable pageable) {
        return converterUtils.convertAll(productRepository.findByPricesPriceAndPricesCurrency(price, currency, pageable), productConverter);
    }

    @Override
    public List<ProductDto> findByCategory(Long id, Pageable pageable) {
        return converterUtils.convertAll(productRepository.findByCategoriesId(id, pageable), productConverter);
    }
}


