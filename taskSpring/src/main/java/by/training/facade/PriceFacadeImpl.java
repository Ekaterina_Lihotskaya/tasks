package by.training.facade;


import by.training.converter.Converter;
import by.training.converter.ConverterUtils;
import by.training.dto.PriceDto;
import by.training.dto.ProductDto;
import by.training.model.Price;
import by.training.model.Product;
import by.training.repository.PriceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PriceFacadeImpl implements PriceFacade {

    private final PriceRepository priceRepository;
    private final Converter<Price, PriceDto> priceConverter;
    private final ConverterUtils converterUtils;


    @Autowired
    public PriceFacadeImpl(PriceRepository priceRepository, Converter<Price, PriceDto> priceConverter, ConverterUtils converterUtils) {
        this.priceRepository = priceRepository;
        this.priceConverter = priceConverter;
        this.converterUtils = converterUtils;
    }

    @Override
    public PriceDto create(PriceDto priceDto) {
        if (priceRepository.findById(priceDto.getId()).isPresent()) {
            Price model = priceConverter.convertDtoToModel(priceDto);
            Price savedModel = priceRepository.save(model);
            return priceConverter.convertModelToDto(savedModel);

        }
        return null;
    }

    @Override
    public PriceDto update(PriceDto priceDto) {
        Price model = priceConverter.convertDtoToModel(priceDto);
        model.setId(priceDto.getId());
        Price savedModel = priceRepository.save(model);
        return priceConverter.convertModelToDto(savedModel);
    }

    @Override
    public void delete(Long id) {
        Price model = new Price();
        model.setId(id);
        priceRepository.delete(model);
    }

    @Override
    public List<PriceDto> findAll(Pageable pageable) {
        return converterUtils.convertAll(priceRepository.findAll(pageable), priceConverter);
    }

    @Override
    public PriceDto findById(Long id) {
        if (priceRepository.findById(id).isPresent()) {
            return priceConverter.convertModelToDto(priceRepository.findById(id).get());
        } else
            return null;
    }

    @Override
    public List<PriceDto> findByCurrency(String currency, Pageable pageable) {
        return converterUtils.convertAll(priceRepository.findByCurrency(currency, pageable), priceConverter);
    }

    @Override
    public List<PriceDto> findPriceByRange(String currency, Pageable pageable, Long start, Long end) {
        return converterUtils.convertAll(priceRepository.findByCurrencyAndPriceBetween(currency, pageable, start, end), priceConverter);
    }

    @Override
    public List<PriceDto> findByProduct(Long codeProduct, Pageable pageable) {
        return converterUtils.convertAll(priceRepository.findByProductsId(codeProduct, pageable), priceConverter);
    }
}
