package by.training.facade;

import by.training.dto.PriceDto;
import org.springframework.data.domain.Pageable;

import java.util.List;


public interface PriceFacade {

    PriceDto create(PriceDto priceDto);

    PriceDto update(PriceDto priceDto);

    void delete(Long id);

    List<PriceDto> findAll(Pageable pageable);

    PriceDto findById(Long id);

    List<PriceDto> findByCurrency(String currency, Pageable pageable);

    List<PriceDto> findPriceByRange(String currency, Pageable pageable, Long start, Long end);

    List<PriceDto> findByProduct(Long codeProduct, Pageable pageable);
}
