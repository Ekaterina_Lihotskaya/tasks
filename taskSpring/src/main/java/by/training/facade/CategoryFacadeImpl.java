package by.training.facade;


import by.training.converter.Converter;
import by.training.converter.ConverterUtils;
import by.training.dto.CategoryDto;
import by.training.model.Category;
import by.training.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CategoryFacadeImpl implements CategoryFacade {

    private final CategoryRepository categoryRepository;
    private final Converter<Category, CategoryDto> categoryConverter;
    private final ConverterUtils converterUtils;

    @Autowired
    public CategoryFacadeImpl(CategoryRepository categoryRepository, Converter<Category, CategoryDto> categoryConverter, ConverterUtils converterUtils) {
        this.categoryRepository = categoryRepository;
        this.categoryConverter = categoryConverter;
        this.converterUtils = converterUtils;
    }

    @Override
    public CategoryDto create(CategoryDto categoryDto) {
        if (categoryRepository.findById(categoryDto.getId()).isPresent()){
            Category model = categoryConverter.convertDtoToModel(categoryDto);
            Category savedModel = categoryRepository.save(model);
            return categoryConverter.convertModelToDto(savedModel);
        }
        else{
            return  null;
        }

    }

    @Override
    public CategoryDto update(CategoryDto categoryDto) {
        Category model = categoryConverter.convertDtoToModel(categoryDto);
        model.setId(categoryDto.getId());
        Category savedModel = categoryRepository.save(model);
        return categoryConverter.convertModelToDto(savedModel);
    }

    @Override
    public void delete(Long id) {
        Category model = new Category();
        model.setId(id);
        categoryRepository.delete(model);
    }

    @Override
    public List<CategoryDto> findAll(Pageable pageable) {
        return converterUtils.convertAll(categoryRepository.findAll(pageable), categoryConverter);
    }

    @Override
    public CategoryDto findById(Long id) {
        if (categoryRepository.findById(id).isPresent()) {
            return categoryConverter.convertModelToDto(categoryRepository.findById(id).get());
        } else
            return null;
    }

    @Override
    public List<CategoryDto> findByName(String name, Pageable pageable) {
        return converterUtils.convertAll(categoryRepository.findByName(name, pageable), categoryConverter);
    }
}
