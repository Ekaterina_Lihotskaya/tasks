package by.training.facade;

import by.training.dto.ProductDto;
import org.springframework.data.domain.Pageable;

import java.util.List;


public interface ProductFacade {

    ProductDto create(ProductDto productDto);

    ProductDto update(ProductDto productDto);

    void delete(Long id);

    ProductDto findById(Long id);

    List<ProductDto> findAll(Pageable pageable);

    List<ProductDto> findByName(String name, Pageable pageable);

    List<ProductDto> findByPrice(Long price, String currency, Pageable pageable);

    List<ProductDto> findByCategory(Long id, Pageable pageable);
}
