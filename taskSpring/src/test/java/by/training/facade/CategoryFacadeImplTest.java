package by.training.facade;

import by.training.converter.CategoryConverter;
import by.training.converter.ConverterUtils;
import by.training.dto.CategoryDto;
import by.training.model.Category;
import by.training.model.Product;
import by.training.repository.CategoryRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CategoryFacadeImplTest {


    @Autowired
    private ApplicationContext context;

    @Autowired
    private CategoryFacadeImpl categoryFacade;

    @MockBean
    private CategoryRepository repository;

    @MockBean
    private CategoryConverter categoryConverter;

    @MockBean
    private ConverterUtils converterUtils;

    private Category category;
    private CategoryDto categoryDto;

    @Before
    public void setUp() throws Exception {
        categoryDto = new CategoryDto();
        categoryDto.setId(10L);
        categoryDto.setName("Category 1");
        category = new Category("Category 1");
        category.setId(10L);
    }

    @Test
    public void create() throws Exception {
        Mockito.when(categoryConverter.convertDtoToModel(categoryDto)).thenReturn(category);
        Mockito.when(repository.save(category)).thenReturn(category);

        categoryFacade.create(categoryDto);


        Mockito.verify(categoryConverter, Mockito.times(1)).convertDtoToModel(categoryDto);
        Mockito.verify(repository, Mockito.times(1)).save(category);
        Mockito.verify(categoryConverter, Mockito.times(1)).convertModelToDto(category);
    }

    @Test
    public void update() throws Exception {
        Mockito.when(categoryConverter.convertDtoToModel(categoryDto)).thenReturn(category);
        Mockito.when(repository.findById(category.getId())).thenReturn(Optional.of(category));
        Mockito.when(repository.save(category)).thenReturn(category);

        categoryFacade.update(categoryDto);

         Mockito.verify(repository, Mockito.times(1)).findById(category.getId());
        Mockito.verify(categoryConverter, Mockito.times(1)).convertDtoToModel(categoryDto);
        Mockito.verify(repository, Mockito.times(1)).save(category);
        Mockito.verify(categoryConverter, Mockito.times(1)).convertModelToDto(category);
    }

    @Test
    public void delete() throws Exception {
        Category category = new Category();
        category.setId(10L);
        categoryFacade.delete(category.getId());
        Mockito.verify(repository, Mockito.times(1)).delete(category);
    }

    @Test
    public void findAll() throws Exception {
        Pageable pageRequest = PageRequest.of(0, 10);
        Page<Category> categories = new PageImpl<>(new ArrayList<Category>());
        Mockito.when(repository.findAll(pageRequest)).thenReturn(categories);
        categoryFacade.findAll(pageRequest);
        Mockito.verify(repository, Mockito.times(1)).findAll(pageRequest);
        Mockito.verify(converterUtils, Mockito.times(1)).convertAll(categories, categoryConverter);
    }

    @Test
    public void findById() throws Exception {
        Long id = categoryDto.getId();
        Mockito.when(repository.findById(id)).thenReturn(Optional.empty());
        categoryFacade.findById(id);
        Mockito.verify(repository, Mockito.times(1)).findById(id);
    }

    @Test
    public void findByName() throws Exception {
        String name =categoryDto.getName();
        Pageable pageRequest = PageRequest.of(0, 10);
        Page<Category> categories = new PageImpl<>(new ArrayList<Category>());
        Mockito.when(repository.findByName(name, pageRequest)).thenReturn(categories);

        categoryFacade.findByName(name, pageRequest);

        Mockito.verify(repository, Mockito.times(1)).findByName(name, pageRequest);
        Mockito.verify(converterUtils, Mockito.times(1)).convertAll(categories, categoryConverter);
    }

}