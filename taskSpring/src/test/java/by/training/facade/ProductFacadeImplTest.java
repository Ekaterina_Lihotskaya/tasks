package by.training.facade;

import by.training.converter.ConverterUtils;
import by.training.converter.ProductConverter;
import by.training.dto.ProductDto;
import by.training.model.Product;
import by.training.repository.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.*;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.parameters.P;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductFacadeImplTest {


    @Autowired
    private ProductFacadeImpl productFacade;

    @MockBean
    private ProductRepository repository;

    @MockBean
    private ProductConverter productConverter;

    @MockBean
    private ConverterUtils converterUtils;

    private Product product;
    private ProductDto productDto;


    @Before
    public void setUp() throws Exception {
        productDto = new ProductDto();
        productDto.setId(10L);
        productDto.setName("Apple");
        productDto.setDescription("This is apple");
        product = new Product("Table", "This is table");
        product.setId(10L);
    }

    @Test
    public void create() throws Exception {
        Mockito.when(productConverter.convertDtoToModel(productDto)).thenReturn(product);
        Mockito.when(repository.save(product)).thenReturn(product);

        productFacade.create(productDto);

        Mockito.verify(productConverter, Mockito.times(1)).convertDtoToModel(productDto);
        Mockito.verify(repository, Mockito.times(1)).save(product);
        Mockito.verify(productConverter, Mockito.times(1)).convertModelToDto(product);
    }


    @Test
    public void update() throws Exception {
        Long id = product.getId();
        Mockito.when(productConverter.convertDtoToModel(productDto)).thenReturn(product);
        Mockito.when(repository.findById(id)).thenReturn(Optional.of(product));
        Mockito.when(repository.save(product)).thenReturn(product);

        productFacade.update(productDto);

        Mockito.verify(repository, Mockito.times(1)).findById(id);
        Mockito.verify(productConverter, Mockito.times(1)).convertDtoToModel(productDto);
        Mockito.verify(repository, Mockito.times(1)).save(product);
        Mockito.verify(productConverter, Mockito.times(1)).convertModelToDto(product);
    }

    @Test
    public void findAll() throws Exception {
        Pageable pageRequest = PageRequest.of(0, 10);
        Page<Product> products = new PageImpl<>(new ArrayList<Product>());
        Mockito.when(repository.findAll(pageRequest)).thenReturn(products);
        productFacade.findAll(pageRequest);
        Mockito.verify(repository, Mockito.times(1)).findAll(pageRequest);
        Mockito.verify(converterUtils, Mockito.times(1)).convertAll(products, productConverter);
    }

    @Test
    public void findById() throws Exception {
        Long id = productDto.getId();
        Mockito.when(repository.findById(id)).thenReturn(Optional.empty());
        productFacade.findById(id);
        Mockito.verify(repository, Mockito.times(1)).findById(id);
    }

    @Test
    public void findByIdOne() throws Exception {
        Long id = productDto.getId();
        Optional<Product> productOptional = Optional.of(product);
        Mockito.when(repository.findById(id)).thenReturn(productOptional);
        productFacade.findById(id);

        Mockito.verify(repository, Mockito.times(2)).findById(id);
        Mockito.verify(productConverter, Mockito.times(1)).convertModelToDto(product);

    }

    @Test
    public void delete() throws Exception {
        Product product = new Product();
        product.setId(10L);
        productFacade.delete(product.getId());
        Mockito.verify(repository, Mockito.times(1)).delete(product);
    }

    @Test
    public void findByName() throws Exception {
        String name = productDto.getName();
        Pageable pageRequest = PageRequest.of(0, 10);
        Page<Product> products = new PageImpl<>(new ArrayList<Product>());
        Mockito.when(repository.findByName(name, pageRequest)).thenReturn(products);

        productFacade.findByName(name, pageRequest);

        Mockito.verify(repository, Mockito.times(1)).findByName(name, pageRequest);
        Mockito.verify(converterUtils, Mockito.times(1)).convertAll(products, productConverter);
    }

    @Test
    public void findByPrice() throws Exception {
        Long price = 100L;
        String currency = "BY";
        Pageable pageRequest = PageRequest.of(0, 10);
        Page<Product> products = new PageImpl<>(new ArrayList<Product>());
        Mockito.when(repository.findByPricesPriceAndPricesCurrency(price, currency, pageRequest)).thenReturn(products);

        productFacade.findByPrice(price, currency, pageRequest);

        Mockito.verify(repository, Mockito.times(1)).findByPricesPriceAndPricesCurrency(price, currency, pageRequest);
        Mockito.verify(converterUtils, Mockito.times(1)).convertAll(products, productConverter);

    }

}