package by.training.facade;

import by.training.converter.ConverterUtils;
import by.training.converter.PriceConverter;
import by.training.converter.ProductConverter;
import by.training.dto.PriceDto;
import by.training.dto.ProductDto;
import by.training.model.Price;
import by.training.model.Product;
import by.training.repository.PriceRepository;
import by.training.repository.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PriceFacadeImplTest {

    @Autowired
    private PriceFacadeImpl priceFacade;

    @MockBean
    private PriceRepository repository;

    @MockBean
    private PriceConverter priceConverter;

    @MockBean
    private ConverterUtils converterUtils;

    private Price price;
    private PriceDto priceDto;

    @Before
    public void setUp() throws Exception {
        priceDto = new PriceDto();
        priceDto.setId(10L);
        priceDto.setPrice(15000L);
        priceDto.setCurrency("BY");
        price = new Price(15000L, "BY");
        price.setId(10L);
    }

    @Test
    public void create() throws Exception {
        Mockito.when(priceConverter.convertDtoToModel(priceDto)).thenReturn(price);
        Mockito.when(repository.save(price)).thenReturn(price);

        priceFacade.create(priceDto);

        Mockito.verify(priceConverter, Mockito.times(1)).convertDtoToModel(priceDto);
        Mockito.verify(repository, Mockito.times(1)).save(price);
        Mockito.verify(priceConverter, Mockito.times(1)).convertModelToDto(price);
    }

    @Test
    public void update() throws Exception {
        Mockito.when(priceConverter.convertDtoToModel(priceDto)).thenReturn(price);
        Mockito.when(repository.findById(price.getId())).thenReturn(Optional.of(price));
        Mockito.when(repository.save(price)).thenReturn(price);

        priceFacade.update(priceDto);

        Mockito.verify(repository, Mockito.times(1)).findById(price.getId());
        Mockito.verify(priceConverter, Mockito.times(1)).convertDtoToModel(priceDto);
        Mockito.verify(repository, Mockito.times(1)).save(price);
        Mockito.verify(priceConverter, Mockito.times(1)).convertModelToDto(price);
    }

    @Test
    public void delete() throws Exception {
        Price price = new Price();
        price.setId(10L);
        priceFacade.delete(price.getId());
        Mockito.verify(repository, Mockito.times(1)).delete(price);
    }

    @Test
    public void findAll() throws Exception {
        Pageable pageRequest = PageRequest.of(0, 10);
        Page<Price> prices = new PageImpl<>(new ArrayList<Price>());
        Mockito.when(repository.findAll(pageRequest)).thenReturn(prices);
        priceFacade.findAll(pageRequest);
        Mockito.verify(repository, Mockito.times(1)).findAll(pageRequest);
        Mockito.verify(converterUtils, Mockito.times(1)).convertAll(prices, priceConverter);
    }

    @Test
    public void findById() throws Exception {
        Long id = priceDto.getId();
        Mockito.when(repository.findById(id)).thenReturn(Optional.empty());
        priceFacade.findById(id);
        Mockito.verify(repository, Mockito.times(1)).findById(id);
    }

    @Test
    public void findByCurrency() throws Exception {
        String currency = price.getCurrency();
        Pageable pageRequest = PageRequest.of(0, 10);
        Page<Price> prices = new PageImpl<>(new ArrayList<Price>());
        Mockito.when(repository.findByCurrency(currency, pageRequest)).thenReturn(prices);

        priceFacade.findByCurrency(currency, pageRequest);

        Mockito.verify(repository, Mockito.times(1)).findByCurrency(currency, pageRequest);
        Mockito.verify(converterUtils, Mockito.times(1)).convertAll(prices, priceConverter);
    }

    @Test
    public void findPriceByRange() throws Exception {
        String currency = price.getCurrency();
        Long start = 100L;
        Long end = 100L;
        Pageable pageRequest = PageRequest.of(0, 10);
        Page<Price> prices = new PageImpl<>(new ArrayList<Price>());
        Mockito.when(repository.findByCurrencyAndPriceBetween(currency, pageRequest, start, end)).thenReturn(prices);

        priceFacade.findPriceByRange(currency, pageRequest, start, end);

        Mockito.verify(repository, Mockito.times(1)).findByCurrencyAndPriceBetween(currency, pageRequest, start, end);
        Mockito.verify(converterUtils, Mockito.times(1)).convertAll(prices, priceConverter);
    }

    @Test
    public void findByProduct() throws Exception {
        Long idProduct = 10L;
        Pageable pageRequest = PageRequest.of(0, 10);
        Page<Price> prices = new PageImpl<>(new ArrayList<Price>());
        Mockito.when(repository.findByProductsId(idProduct, pageRequest)).thenReturn(prices);

        priceFacade.findByProduct(idProduct, pageRequest);

        Mockito.verify(repository, Mockito.times(1)).findByProductsId(idProduct, pageRequest);
        Mockito.verify(converterUtils, Mockito.times(1)).convertAll(prices, priceConverter);
    }
}