package by.training.web.controller;

import by.training.dto.ProductDto;
import by.training.facade.ProductFacadeImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProductRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductFacadeImpl productFacade;

    private ProductDto mockProduct;
    private String inputInJson;


    @Before
    public void setUp() throws Exception {
        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken("tom", "123");
        SecurityContextHolder.getContext().setAuthentication(auth);
        mockProduct = new ProductDto();
        mockProduct.setId(10L);
        mockProduct.setName("Apple");
        mockProduct.setDescription("This is apple");
        inputInJson = this.mapToJson(mockProduct);
    }

    @Test
    public void createProductBadRequest() throws Exception {
        this.mockMvc.perform(post("/product"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void createProductOk() throws Exception {
        String URI = "/product";

        Mockito.when(productFacade.create(mockProduct)).thenReturn(mockProduct);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post(URI)
                .accept(MediaType.APPLICATION_JSON).content(inputInJson)
                .contentType(MediaType.APPLICATION_JSON);
        checkResult(inputInJson, requestBuilder);
    }

    @Test
    public void updateProductBadRequest() throws Exception {
        this.mockMvc.perform(put("/product"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void updateProduct() throws Exception {
        String URI = "/product";

        Mockito.when(productFacade.findById(Mockito.anyLong())).thenReturn(mockProduct);
        Mockito.when(productFacade.update(mockProduct)).thenReturn(mockProduct);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .put(URI)
                .accept(MediaType.APPLICATION_JSON).content(inputInJson)
                .contentType(MediaType.APPLICATION_JSON);
        checkResult(inputInJson, requestBuilder);
    }

    @Test
    public void deleteProductNotFound() throws Exception {
        String URI = "/product/1";
        Mockito.when(productFacade.findById(Mockito.anyLong())).thenReturn(null);
        this.mockMvc.perform(delete(URI))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteProduct() throws Exception {
        String URI = "/product/1";
        Mockito.when(productFacade.findById(Mockito.anyLong())).thenReturn(mockProduct);
        this.mockMvc.perform(delete(URI))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getAllProductNoContent() throws Exception {
        List<ProductDto> list = new ArrayList<>();
        String URI = "/product";

        Mockito.when(productFacade.findAll(Mockito.any(Pageable.class))).thenReturn(list);
        this.mockMvc.perform(get(URI))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    public void getAllProduct() throws Exception {
        List<ProductDto> list = getListProductDto();
        String URI = "/product";

        Mockito.when(productFacade.findAll(Mockito.any(Pageable.class))).thenReturn(list);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI)
                .accept(MediaType.APPLICATION_JSON);
        checkResult(inputInJson, requestBuilder);
    }

    @Test
    public void getProductByIdNoContent() throws Exception {
        String URI = "/product/search/1";

        Mockito.when(productFacade.findById(Mockito.anyLong())).thenReturn(null);
        this.mockMvc.perform(get(URI))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void getProductById() throws Exception {
        String URI = "/product/search/1";

        Mockito.when(productFacade.findById(Mockito.anyLong())).thenReturn(mockProduct);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI)
                .accept(MediaType.APPLICATION_JSON);
        checkResult(inputInJson, requestBuilder);
    }

    @Test
    public void getProductByNameBadRequest() throws Exception {
        String URI = "/product/search";
        String name = null;
        this.mockMvc.perform(get(URI).accept(MediaType.APPLICATION_JSON).param("name", name))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getProductByNameNoContent() throws Exception {
        List<ProductDto> list = new ArrayList<>();
        String URI = "/product/search";

        Mockito.when(productFacade.findByName(Mockito.anyString(), Mockito.any(Pageable.class))).thenReturn(list);

        this.mockMvc.perform(get(URI).accept(MediaType.APPLICATION_JSON).param("name", mockProduct.getName()))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    public void getProductByName() throws Exception {
        List<ProductDto> list = getListProductDto();
        String URI = "/product/search";

        Mockito.when(productFacade.findByName(Mockito.anyString(), Mockito.any(Pageable.class))).thenReturn(list);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI)
                .accept(MediaType.APPLICATION_JSON).param("name", mockProduct.getName());
        checkResult(inputInJson, requestBuilder);
    }

    @Test
    public void getProductByPriceBadRequest() throws Exception {
        String URI = "/product/search";
        String price = null;
        String currency = null;
        this.mockMvc.perform(get(URI).accept(MediaType.APPLICATION_JSON).param("price", price)
                .param("currency", currency))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getProductByPriceNoContent() throws Exception {
        List<ProductDto> list = new ArrayList<>();
        String currency = "BY";
        String price = "100";
        String URI = "/product/search";

        Mockito.when(productFacade.findByName(Mockito.anyString(), Mockito.any(Pageable.class))).thenReturn(list);

        this.mockMvc.perform(get(URI).accept(MediaType.APPLICATION_JSON).param("price", price)
                .param("currency", currency))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    public void getProductByPrice() throws Exception {
        List<ProductDto> list = getListProductDto();
        String currency = "BY";
        String price = "100";
        String URI = "/product/search";

        Mockito.when(productFacade.findByPrice(Mockito.anyLong(), Mockito.anyString(), Mockito.any(Pageable.class))).thenReturn(list);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI)
                .accept(MediaType.APPLICATION_JSON).param("price", price)
                .param("currency", currency);
        checkResult(inputInJson, requestBuilder);

    }

    @Test
    public void getProductByCategoryBadRequest() throws Exception {
        String URI = "/product/search";
        String idCategory = null;
        this.mockMvc.perform(get(URI).accept(MediaType.APPLICATION_JSON).param("idCategory", idCategory))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getProductByCategoryNoContent() throws Exception {
        List<ProductDto> list = new ArrayList<>();
        String idCategory = "99";
        String URI = "/product/search";

        Mockito.when(productFacade.findByName(Mockito.anyString(), Mockito.any(Pageable.class))).thenReturn(list);

        this.mockMvc.perform(get(URI).accept(MediaType.APPLICATION_JSON).param("idCategory", idCategory))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    public void getProductByCategory() throws Exception {
        List<ProductDto> list = getListProductDto();
        String idCategory = "99";
        String URI = "/product/search";

        Mockito.when(productFacade.findByCategory(Mockito.anyLong(), Mockito.any(Pageable.class))).thenReturn(list);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI)
                .accept(MediaType.APPLICATION_JSON)
                .param("idCategory", idCategory);
        checkResult(inputInJson, requestBuilder);
    }


    private String mapToJson(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(object);
    }

    private void checkResult(String inputInJson, RequestBuilder requestBuilder) throws Exception {
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();
        String outputInJson = response.getContentAsString();
        assertThat(outputInJson).isEqualTo(inputInJson);
        assertEquals(HttpStatus.OK.value(), response.getStatus());
    }

    private List<ProductDto> getListProductDto() throws Exception {
        List<ProductDto> list = new ArrayList<>();
        list.add(mockProduct);
        inputInJson = this.mapToJson(list);
        return list;
    }
}

