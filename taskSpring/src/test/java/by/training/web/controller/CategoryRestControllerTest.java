package by.training.web.controller;

import by.training.dto.CategoryDto;
import by.training.dto.ProductDto;
import by.training.facade.CategoryFacade;
import by.training.facade.ProductFacadeImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CategoryRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CategoryFacade categoryFacade;

    private CategoryDto mockCategory;
    private String inputInJson;


    @Before
    public void setUp() throws Exception {
        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken("tom", "123");
        SecurityContextHolder.getContext().setAuthentication(auth);
        mockCategory = new CategoryDto();
        mockCategory.setId(10L);
        mockCategory.setName("Food");
        inputInJson = this.mapToJson(mockCategory);
    }


    @Test
    public void createCategoryBadRequest() throws Exception {
        this.mockMvc.perform(post("/category"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void createCategoryOk() throws Exception {
        String URI = "/category";

        Mockito.when(categoryFacade.create(mockCategory)).thenReturn(mockCategory);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post(URI)
                .accept(MediaType.APPLICATION_JSON).content(inputInJson)
                .contentType(MediaType.APPLICATION_JSON);
        checkResult(inputInJson, requestBuilder);
    }

    @Test
    public void updateCategoryBadRequest() throws Exception {
        this.mockMvc.perform(put("/category"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void updateCategory() throws Exception {
        String URI = "/category";

        Mockito.when(categoryFacade.findById(Mockito.anyLong())).thenReturn(mockCategory);
        Mockito.when(categoryFacade.update(mockCategory)).thenReturn(mockCategory);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .put(URI)
                .accept(MediaType.APPLICATION_JSON).content(inputInJson)
                .contentType(MediaType.APPLICATION_JSON);
        checkResult(inputInJson, requestBuilder);
    }

    @Test
    public void deleteCategoryNotFound() throws Exception {
        String URI = "/category/1";
        Mockito.when(categoryFacade.findById(Mockito.anyLong())).thenReturn(null);
        this.mockMvc.perform(delete(URI))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteCategory() throws Exception {
        String URI = "/category/1";
        Mockito.when(categoryFacade.findById(Mockito.anyLong())).thenReturn(mockCategory);
        this.mockMvc.perform(delete(URI))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getAllCategoryNoContent() throws Exception {
        List<CategoryDto> list = new ArrayList<>();
        String URI = "/category";

        Mockito.when(categoryFacade.findAll(Mockito.any(Pageable.class))).thenReturn(list);
        this.mockMvc.perform(get(URI))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    public void getAllCategory() throws Exception {
        List<CategoryDto> list = getListCategoryDto();
        String URI = "/category";

        Mockito.when(categoryFacade.findAll(Mockito.any(Pageable.class))).thenReturn(list);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI)
                .accept(MediaType.APPLICATION_JSON);
        checkResult(inputInJson, requestBuilder);
    }

    @Test
    public void getCategoryByIdNoContent() throws Exception {
        String URI = "/category/search/1";

        Mockito.when(categoryFacade.findById(Mockito.anyLong())).thenReturn(null);
        this.mockMvc.perform(get(URI))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void getCategoryById() throws Exception {
        String URI = "/category/search/1";

        Mockito.when(categoryFacade.findById(Mockito.anyLong())).thenReturn(mockCategory);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI)
                .accept(MediaType.APPLICATION_JSON);
        checkResult(inputInJson, requestBuilder);
    }

    @Test
    public void getCategoryByNameBadRequest() throws Exception {
        String URI = "/category/search";
        String name = null;
        this.mockMvc.perform(get(URI).accept(MediaType.APPLICATION_JSON).param("name", name))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getCategoryByNameNoContent() throws Exception {
        List<CategoryDto> list = new ArrayList<>();
        String URI = "/category/search";

        Mockito.when(categoryFacade.findByName(Mockito.anyString(), Mockito.any(Pageable.class))).thenReturn(list);

        this.mockMvc.perform(get(URI).accept(MediaType.APPLICATION_JSON).param("name", mockCategory.getName()))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    public void getCategoryByName() throws Exception {
        List<CategoryDto> list = getListCategoryDto();
        String URI = "/category/search";

        Mockito.when(categoryFacade.findByName(Mockito.anyString(), Mockito.any(Pageable.class))).thenReturn(list);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI)
                .accept(MediaType.APPLICATION_JSON).param("name", mockCategory.getName());
        checkResult(inputInJson, requestBuilder);
    }


    private String mapToJson(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(object);
    }

    private void checkResult(String inputInJson, RequestBuilder requestBuilder) throws Exception {
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();
        String outputInJson = response.getContentAsString();
        assertThat(outputInJson).isEqualTo(inputInJson);
        assertEquals(HttpStatus.OK.value(), response.getStatus());
    }

    private List<CategoryDto> getListCategoryDto() throws Exception {
        List<CategoryDto> list = new ArrayList<>();
        list.add(mockCategory);
        inputInJson = this.mapToJson(list);
        return list;
    }
}

