package by.training.web.controller;

import by.training.dto.PriceDto;
import by.training.dto.ProductDto;
import by.training.facade.PriceFacadeImpl;
import by.training.facade.ProductFacadeImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PriceRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PriceFacadeImpl priceFacade;

    private PriceDto mockPrice;
    private String inputInJson;


    @Before
    public void setUp() throws Exception {
        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken("tom", "123");
        SecurityContextHolder.getContext().setAuthentication(auth);
        mockPrice = new PriceDto();
        mockPrice.setId(10L);
        mockPrice.setCurrency("BY");
        mockPrice.setPrice(4500L);
        inputInJson = this.mapToJson(mockPrice);
    }


    @Test
    public void createPriceBadRequest() throws Exception {
        this.mockMvc.perform(post("/price"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void createPriceOk() throws Exception {
        String URI = "/price";

        Mockito.when(priceFacade.create(mockPrice)).thenReturn(mockPrice);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post(URI)
                .accept(MediaType.APPLICATION_JSON).content(inputInJson)
                .contentType(MediaType.APPLICATION_JSON);
        checkResult(inputInJson, requestBuilder);
    }

    @Test
    public void updatePriceBadRequest() throws Exception {
        this.mockMvc.perform(put("/price"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void updatePrice() throws Exception {
        String URI = "/price";

        Mockito.when(priceFacade.findById(Mockito.anyLong())).thenReturn(mockPrice);
        Mockito.when(priceFacade.update(mockPrice)).thenReturn(mockPrice);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .put(URI)
                .accept(MediaType.APPLICATION_JSON).content(inputInJson)
                .contentType(MediaType.APPLICATION_JSON);
        checkResult(inputInJson, requestBuilder);
    }

    @Test
    public void deletePriceNotFound() throws Exception {
        String URI = "/price/1";
        Mockito.when(priceFacade.findById(Mockito.anyLong())).thenReturn(null);
        this.mockMvc.perform(delete(URI))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void deletePrice() throws Exception {
        String URI = "/price/1";
        Mockito.when(priceFacade.findById(Mockito.anyLong())).thenReturn(mockPrice);
        this.mockMvc.perform(delete(URI))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getAllPriceNoContent() throws Exception {
        List<PriceDto> list = new ArrayList<>();
        String URI = "/price";

        Mockito.when(priceFacade.findAll(Mockito.any(Pageable.class))).thenReturn(list);
        this.mockMvc.perform(get(URI))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    public void getAllPrice() throws Exception {
        List<PriceDto> list = getListPriceDto();
        String URI = "/price";

        Mockito.when(priceFacade.findAll(Mockito.any(Pageable.class))).thenReturn(list);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI)
                .accept(MediaType.APPLICATION_JSON);
        checkResult(inputInJson, requestBuilder);
    }

    @Test
    public void getPriceByIdNoContent() throws Exception {
        String URI = "/price/search/1";

        Mockito.when(priceFacade.findById(Mockito.anyLong())).thenReturn(null);
        this.mockMvc.perform(get(URI))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void getPriceById() throws Exception {
        String URI = "/price/search/1";

        Mockito.when(priceFacade.findById(Mockito.anyLong())).thenReturn(mockPrice);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI)
                .accept(MediaType.APPLICATION_JSON);
        checkResult(inputInJson, requestBuilder);
    }


    @Test
    public void getPriceByCurrencyBadRequest() throws Exception {
        String URI = "/price/search";
        String currency = null;
        this.mockMvc.perform(get(URI).accept(MediaType.APPLICATION_JSON).param("currency", currency))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getPriceByCurrencyNoContent() throws Exception {
        List<PriceDto> list = new ArrayList<>();
        String URI = "/price/search";

        Mockito.when(priceFacade.findByCurrency(Mockito.anyString(), Mockito.any(Pageable.class))).thenReturn(list);

        this.mockMvc.perform(get(URI).accept(MediaType.APPLICATION_JSON).param("currency", mockPrice.getCurrency()))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    public void getPriceByCurrency() throws Exception {
        List<PriceDto> list = getListPriceDto();
        String URI = "/price/search";

        Mockito.when(priceFacade.findByCurrency(Mockito.anyString(), Mockito.any(Pageable.class))).thenReturn(list);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI)
                .accept(MediaType.APPLICATION_JSON).param("currency", mockPrice.getCurrency());
        checkResult(inputInJson, requestBuilder);
    }


    @Test
    public void getPriceByRangeBadRequest() throws Exception {
        String URI = "/price/search";
        String start = "5";
        String end = null;
        this.mockMvc.perform(get(URI).accept(MediaType.APPLICATION_JSON)
                .param("currency", mockPrice.getCurrency())
                .param("start", start)
                .param("end", end))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getPriceByRangeNoContent() throws Exception {
        List<PriceDto> list = new ArrayList<>();
        String URI = "/price/search";
        String start = "5";
        String end = "10";
        Mockito.when(priceFacade.findPriceByRange(Mockito.anyString(), Mockito.any(Pageable.class), Mockito.anyLong(), Mockito.anyLong())).thenReturn(list);

        this.mockMvc.perform(get(URI).accept(MediaType.APPLICATION_JSON)
                .param("currency", mockPrice.getCurrency())
                .param("start", start)
                .param("end", end))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    public void getPriceByRange() throws Exception {
        List<PriceDto> list = getListPriceDto();
        String URI = "/price/search";
        String start = "5";
        String end = "10";

        Mockito.when(priceFacade.findPriceByRange(Mockito.anyString(), Mockito.any(Pageable.class), Mockito.anyLong(), Mockito.anyLong())).thenReturn(list);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI)
                .accept(MediaType.APPLICATION_JSON)
                .param("currency", mockPrice.getCurrency())
                .param("start", start)
                .param("end", end);
        checkResult(inputInJson, requestBuilder);
    }


    @Test
    public void getPriceByProductBadRequest() throws Exception {
        String URI = "/price/search";
        String idProduct = null;
        this.mockMvc.perform(get(URI).accept(MediaType.APPLICATION_JSON).param("idProduct", idProduct))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getPriceByProductNoContent() throws Exception {
        List<PriceDto> list = new ArrayList<>();
        String URI = "/price/search";
        String idProduct = "10";

        Mockito.when(priceFacade.findByProduct(Mockito.anyLong(), Mockito.any(Pageable.class))).thenReturn(list);

        this.mockMvc.perform(get(URI).accept(MediaType.APPLICATION_JSON).param("idProduct", idProduct))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    public void getPriceByProduct() throws Exception {
        List<PriceDto> list = getListPriceDto();
        String URI = "/price/search";
        String idProduct = "10";

        Mockito.when(priceFacade.findByProduct(Mockito.anyLong(), Mockito.any(Pageable.class))).thenReturn(list);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI)
                .accept(MediaType.APPLICATION_JSON).param("idProduct", idProduct);
        checkResult(inputInJson, requestBuilder);
    }


    private String mapToJson(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(object);
    }

    private void checkResult(String inputInJson, RequestBuilder requestBuilder) throws Exception {
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();
        String outputInJson = response.getContentAsString();
        assertThat(outputInJson).isEqualTo(inputInJson);
        assertEquals(HttpStatus.OK.value(), response.getStatus());
    }

    private List<PriceDto> getListPriceDto() throws Exception {
        List<PriceDto> list = new ArrayList<>();
        list.add(mockPrice);
        inputInJson = this.mapToJson(list);
        return list;
    }
}

