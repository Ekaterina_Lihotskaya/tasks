<%@ page import="by.gsu.epamlab.constants.ConstantsJSP" %>
<%@ taglib uri="/jstl/core" prefix="c" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF8">
    <link rel="stylesheet" type="text/css" href="stylesheet.css">
    <title>Login</title></head>
<body>
<c:if test="${not empty errorMessage}">
    <c:out value="${errorMessage}"/>
    <hr>
</c:if>
<div class="main">
    <div class="container2">
        <form name="loginForm" method="POST" action="/registrate">
            Name:<br>
            <input type="text" name=<%= ConstantsJSP.KEY_NAME %> value=""><br> <br>
            Login:<br>
            <input type="text" name=<%= ConstantsJSP.KEY_LOGIN %> value=""><br><br>
            Password:<br>
            <input type="password" name=<%= ConstantsJSP.KEY_PASSWORD %> value=""><br><br>
            Phone:<br>
            <input type="text" name=<%= ConstantsJSP.KEY_PHONE %> value=""><br><br>
            E-mail:<br>
            <input type="email" name=<%= ConstantsJSP.KEY_E_MAIL %> value=""><br><br>
            <input type="submit" value="Enter"> &nbsp;&nbsp;&nbsp;
            <a href="index.jsp">Start page</a>
        </form>
    </div>
</div>
</body>
</html>
