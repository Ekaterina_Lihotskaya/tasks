<%@ page import="by.gsu.epamlab.constants.ConstantsJSP" %>
<%@ taglib uri="/jstl/core" prefix="c" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF8">
    <link rel="stylesheet" type="text/css" href="stylesheet.css">
    <title>Add task</title></head>
<body>
<div class="main">
    <div class="container3">
        <form name="add task" method="POST" action="/add" enctype="MULTIPART/FORM-DATA">

            Description:<br>
            <input type="text" name=<%= ConstantsJSP.KEY_DESCRIPTION %> value=""><br> <br>
            <input type="hidden" name=<%= ConstantsJSP.KEY_SECTION %> value="${param.add}">
            <c:if test="${param.add eq 'someday'}">
                Date:&nbsp;<br>
             <input type="date" name=<%= ConstantsJSP.KEY_DATE %> value=""><br> <br>
            </c:if>

            Select file to upload <input type = FILE  name = "file name">
<BR><BR>
            <input type="submit" value="Add task"> &nbsp;&nbsp;&nbsp;

            <a href = "update.jsp">Return</a>


        </form>
    </div>
</div>
</body>
</html>
