<%@ taglib uri="/jstl/core" prefix="c" %>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF8">
    <link rel="stylesheet" type="text/css" href="stylesheet.css">
    <title>Tasks</title>
	
	<script>
	function actionFile(servlet, idTask){
                document.chooseAction.idTask.value=idTask;
                chooseAction.action = servlet;
                chooseAction.submit();
            };
	</script>
	

</head>
<body>
<%@ include file="header.jsp" %>
<br>
<div class="main">
    <div class="container1">
        <div class="left-sidebar">
            <div class="left-side">
                <h3> Sections of the list </h3>
                <form name="chooseSection" method="post" action="/update">
                    <input type="submit" name="section" value="Today"> </br> </br>
                    <input type="submit" name="section" value="Tomorrow"> </br> </br>
                    <input type="submit" name="section" value="Someday"> </br> </br>
                    <input type="submit" name="section" value="Fixed"> </br> </br>
                    <button name="section" value="Recycle_Bin" type="submit">Recycle Bin</button>
                    </br> </br>
                </form>
            </div>
        </div>

        <div class="content">
            <H1 align="center"> ${param.section}
                <c:if test="${empty param.section}">
                    Today
                </c:if></H1>

            <form name="chooseAction" method="post" action='/action'>
                <input type="hidden" name="section" value= ${param.section}>
                <table width=100% border="1" align="center">
                    <tr>
                        <th>checkbox</th>
                        <th>discription</th>
                        <c:if test="${not(param.section eq 'Today' or param.section eq 'Tomorrow' or empty param.section)}"
                              var='condition'
                              scope='page'>
                            <th>date</th>
                        </c:if>
                        <th>file</th>
                    </tr>
                    <c:forEach var="task" items="${tasks}">
                        <tr>
                            <td><input type="checkbox" name="idTaskCheckbox" value="${task.idTask}"></td>
                            <td>${task.description}</td>
                            <c:if test="${condition}">
                                <td>${task.date} </td>
                            </c:if>
                            <td>
                            <c:if test="${not(empty task.fileName)}">
                            "${task.fileName}" <br>

                            <a href="JavaScript:actionFile('/download', '${task.idTask}' )">Download</a>&nbsp;&nbsp;
                            <a href="JavaScript:actionFile('/delete', '${task.idTask}' )">Delete</a> &nbsp;&nbsp;
                            </c:if>
                                <a href="JavaScript:actionFile('/upload.jsp', '${task.idTask}' )">Upload</a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
                <input type="hidden" name="idTask"}>

                <c:choose>
                    <c:when test="${param.section eq 'Fixed'}">
                        <button name="action" value="recover" type="submit">Recover</button>
                        <button name="action" value="delete" type="submit">Delete task</button>
                    </c:when>
                    <c:when test="${param.section eq 'Recycle_Bin'}">
                        <button name="action" value="remove" type="submit">Remove task from Recycle Bin</button>
                        <button name="action" value="remove_all" type="submit">Empty Rycycle Bin</button>
                        <button name="action" value="recover" type="submit">Recover</button>
                    </c:when>
                    <c:otherwise>
                        <button name="action" value="delete" type="submit">Delete task</button>
                        <button name="action" value="fixed" type="submit">Put to Fixed</button>
                    </c:otherwise>
                </c:choose>
            </form>
            <form name="Add" action="add.jsp">
                <button name="add" value="today" type="submit">Add task to Today</button>
                <button name="add" value="tomorrow" type="submit">Add task to Tomorrow</button>
                <button name="add" value="someday" type="submit">Add task task to Someday</button>
            </form>
        </div>
    </div>
</div>
<%@include file="footer.html" %>
</body>
</html>
