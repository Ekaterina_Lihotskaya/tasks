<header>

    <c:if test="${not empty errorMessage}">
        <c:out value="${errorMessage}"/>
        <hr>
    </c:if>

    <c:choose>
        <c:when test="${not empty user}">
            <form name='log' method="POST" action="/logout"></form>
            <p class="left"> User:&nbsp;${user.login}</p>
            <a class="right" href="JavaScript:document.log.submit()"> Logout </a>
        </c:when>
        <c:otherwise>
            <div class="container">
                User:&nbsp;<b>guest</b>&nbsp;&nbsp;&nbsp;
                <a href="login.jsp">Login</a>&nbsp;&nbsp;&nbsp;
                <a href="registrate.jsp">Registrate</a>
            </div>
        </c:otherwise>
    </c:choose>
</header>

