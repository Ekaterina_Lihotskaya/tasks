<%@ page import="by.gsu.epamlab.constants.ConstantsJSP" %>
<%@ taglib uri="/jstl/core" prefix="c" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF8">
    <link rel="stylesheet" type="text/css" href="stylesheet.css">
    <title>Login</title></head>

<body>
<c:if test="${not empty errorMessage}">
    <c:out value="${errorMessage}"/>
    <hr>
</c:if>
<div class="main">
    <div class="container2">
        <form name="loginForm" method="POST" action="/login">
            Login:<br>
            <input type="text" name=<%= ConstantsJSP.KEY_LOGIN %> value=""><br><br>
            Password:<br>
            <input type="password" name=<%= ConstantsJSP.KEY_PASSWORD %> value=""><br><br>
            <input type="submit" value="Enter">
            <br><br><br><a href="registrate.jsp">Registrate</a>
        </form>
    </div>
</div>
</body>
</html>
