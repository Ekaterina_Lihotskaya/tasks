<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/c.tld" prefix="c" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="stylesheet.css">
    <title>Start page</title>
</head>
<body>
<%@ include file="header.jsp" %>
<div class="main">
    <div class="container1">
        <h1> An application is a ToDo list with the ability to attach a file to each task in the list. <br>
            The list contains three sections: Today, Tomorrow, and Someday.<br>
            Each section can contain an unlimited number of tasks. <br>
            You can attach one file to each task. <br>
            In order to enter the application, you need to register or authenticate.<br>
        </h1>
    </div>
</div>
<%@include file="footer.html" %>
</body>
</html>