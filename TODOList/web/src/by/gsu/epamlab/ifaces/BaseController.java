package by.gsu.epamlab.ifaces;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.gsu.epamlab.constants.Constants;
import by.gsu.epamlab.model.factories.TaskFactory;
import by.gsu.epamlab.model.factories.UserFactory;

public abstract class BaseController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        String strUserDAO = getServletContext().getInitParameter(Constants.KEY_USER_DAO);
        String strTaskDAO = getServletContext().getInitParameter(Constants.KEY_TASK_DAO);
        UserFactory.setUserDAO(strUserDAO);
        TaskFactory.setTaskDAO(strTaskDAO);
    }

    protected void forward(String url, HttpServletRequest request,
                           HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(url);
        requestDispatcher.forward(request, response);
    }

    protected void forwardError(String message, HttpServletRequest request,
                                HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute(Constants.KEY_ERROR_MESSAGE, message);
        forward(Constants.JUMP_INDEX, request, response);
    }

    protected void forwardError(String message, String url, HttpServletRequest request,
                                HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute(Constants.KEY_ERROR_MESSAGE, message);
        forward(url, request, response);
    }
}
