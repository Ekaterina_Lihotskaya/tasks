package by.gsu.epamlab.ifaces;

import by.gsu.epamlab.model.beans.Task;
import by.gsu.epamlab.model.beans.User;
import by.gsu.epamlab.model.enums.Action;
import by.gsu.epamlab.model.exceptions.DaoException;

import java.util.List;

public interface ITaskDAO {
	List<Task> getTasks(User user, String section);

	void actionByIdTask(Action action, String [] idTasks) throws DaoException;
	void removeAll (User user) throws DaoException;
	int add (Task task, User user);
	void deleteFile(int idTask);
	String getFileName(int idTask);
	void setFileName(String filename, int idTask);


}
