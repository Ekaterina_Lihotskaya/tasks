package by.gsu.epamlab.ifaces;


public interface IUserDAO {
	boolean getUser(String login, String password);
	boolean addUser(String name, String login, String password, String phone, String eMail);
}
