package by.gsu.epamlab.controllers.userControllers;

import by.gsu.epamlab.constants.Constants;
import by.gsu.epamlab.constants.ConstantsJSP;
import by.gsu.epamlab.ifaces.BaseController;
import by.gsu.epamlab.ifaces.IUserDAO;
import by.gsu.epamlab.model.beans.User;
import by.gsu.epamlab.model.factories.UserFactory;
import by.gsu.epamlab.model.exceptions.DaoException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RegistrateController extends BaseController {
    private static final Logger LOGGER = Logger.getLogger(RegistrateController.class.getName());

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter(ConstantsJSP.KEY_NAME);
        String login = request.getParameter(ConstantsJSP.KEY_LOGIN);
        String password = request.getParameter(ConstantsJSP.KEY_PASSWORD);
        String phone = request.getParameter(ConstantsJSP.KEY_PHONE);
        String eMail = request.getParameter(ConstantsJSP.KEY_E_MAIL);

        String inputResult = getInputResult(name, login, password, phone, eMail);
        if (!Constants.OK.equals(inputResult)) {
            forwardError(inputResult, Constants.JUMP_REGISTRATE,
                    request, response);
            return;
        }
        try {
            IUserDAO userDAO = UserFactory.getClassFromFactory();

            if (userDAO.addUser(name, login, password, phone, eMail)) {
                User user = new User(login, password);
                HttpSession session = request.getSession();
                session.setAttribute(Constants.KEY_USER, user);
                forward(Constants.JUMP_UPDATE, request, response);
            } else {
                forwardError(Constants.ERROR_USER_EXIST, Constants.JUMP_REGISTRATE, request, response);
            }
        } catch (DaoException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            forwardError(Constants.ERROR_DAO, Constants.JUMP_INDEX, request, response);
        }
    }

    private String getInputResult(String name, String login, String password, String phone, String eMail) {
        if (name == null || login == null || password == null || phone == null || eMail == null) {
            return Constants.ERROR_NULL;
        }
        name = name.trim();
        login = login.trim();
        password = password.trim();
        phone = phone.trim();
        eMail = eMail.trim();
        if (Constants.EMPTY.equals(login) || Constants.EMPTY.equals(name) ||
                Constants.EMPTY.equals(password) || Constants.EMPTY.equals(phone) ||
                Constants.EMPTY.equals(eMail)) {
            return Constants.ERROR_EMPTY;
        }
        return Constants.OK;
    }
}