package by.gsu.epamlab.controllers.userControllers;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.gsu.epamlab.constants.Constants;
import by.gsu.epamlab.constants.ConstantsJSP;
import by.gsu.epamlab.model.beans.User;
import by.gsu.epamlab.ifaces.BaseController;
import by.gsu.epamlab.ifaces.IUserDAO;
import by.gsu.epamlab.model.factories.UserFactory;
import by.gsu.epamlab.model.exceptions.DaoException;


public class LoginController extends BaseController {
    private static final Logger LOGGER = Logger.getLogger(LoginController.class.getName());

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String login = request.getParameter(ConstantsJSP.KEY_LOGIN);
        String password = request.getParameter(ConstantsJSP.KEY_PASSWORD);

        String inputResult = getInputResult(login, password);
        if (!Constants.OK.equals(inputResult)) {
            forwardError(inputResult, Constants.JUMP_LOGIN,
                    request, response);
            return;
        }

        try {
            IUserDAO userDAO = UserFactory.getClassFromFactory();

            if (userDAO.getUser(login, password)) {
                User user = new User(login, password);
                HttpSession session = request.getSession();
                session.setAttribute(Constants.KEY_USER, user);
                forward(Constants.JUMP_UPDATE, request, response);
            } else {
                forwardError(Constants.ERROR_PASSWORD_LOGIN, Constants.JUMP_LOGIN, request, response);
            }
        } catch (DaoException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            forwardError(Constants.ERROR_DAO, Constants.JUMP_INDEX, request, response);
        }
    }

    private String getInputResult(String login, String password) {
        if (login == null || password == null) {
            return Constants.ERROR_NULL;
        }
        login = login.trim();
        password = password.trim();
        if (Constants.EMPTY.equals(login) || Constants.EMPTY.equals(password)) {
            return Constants.ERROR_EMPTY;
        }
        return Constants.OK;
    }
}
