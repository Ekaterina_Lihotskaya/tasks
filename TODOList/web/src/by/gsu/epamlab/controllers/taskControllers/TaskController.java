package by.gsu.epamlab.controllers.taskControllers;

import by.gsu.epamlab.constants.Constants;
import by.gsu.epamlab.constants.ConstantsJSP;
import by.gsu.epamlab.ifaces.BaseController;
import by.gsu.epamlab.ifaces.ITaskDAO;
import by.gsu.epamlab.model.beans.Task;
import by.gsu.epamlab.model.beans.User;
import by.gsu.epamlab.model.exceptions.DaoException;
import by.gsu.epamlab.model.factories.TaskFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TaskController extends BaseController {
    private static final Logger LOGGER = Logger.getLogger(TaskController.class.getName());

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        User user = (User) session.getAttribute(Constants.KEY_USER);
        String section = request.getParameter(ConstantsJSP.KEY_SECTION);

        if (section == null || section.isEmpty()) {
            section = Constants.DEFAULT_SECTION;

        }
        try {
            ITaskDAO taskDAO = TaskFactory.getClassFromFactory();
            List<Task> tasks = taskDAO.getTasks(user, section);
            request.setAttribute("tasks", tasks);

            forward(Constants.JUMP_UPDATE_JSP, request, response);
        } catch (DaoException e)

        {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            forwardError(Constants.JUMP_INDEX, request, response);
        }

    }
}