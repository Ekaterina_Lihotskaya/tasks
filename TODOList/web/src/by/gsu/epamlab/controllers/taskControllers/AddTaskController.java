package by.gsu.epamlab.controllers.taskControllers;

import by.gsu.epamlab.constants.Constants;
import by.gsu.epamlab.constants.ConstantsJSP;
import by.gsu.epamlab.ifaces.BaseController;
import by.gsu.epamlab.ifaces.ITaskDAO;
import by.gsu.epamlab.model.beans.Task;
import by.gsu.epamlab.model.beans.User;
import by.gsu.epamlab.model.enums.Section;
import by.gsu.epamlab.model.exceptions.DaoException;
import by.gsu.epamlab.model.factories.TaskFactory;
import by.gsu.epamlab.utils.FileLoadManager;

import javax.servlet.ServletException;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Date;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

@MultipartConfig
public class AddTaskController extends BaseController {
    private static final Logger LOGGER = Logger.getLogger(AddTaskController.class.getName());

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        try {
            User user = (User) session.getAttribute(Constants.KEY_USER);
            String section = request.getParameter(ConstantsJSP.KEY_SECTION);
            String dateStr = request.getParameter(ConstantsJSP.KEY_DATE);
            String description = request.getParameter(ConstantsJSP.KEY_DESCRIPTION);
            String fileName;
            Date date;

            if (dateStr != null) {
                date = Date.valueOf(dateStr);
            } else {
                date = getDate(section);
            }

            Task task = new Task(description, date);
            ITaskDAO taskDAO = TaskFactory.getClassFromFactory();

            int idTask = taskDAO.add(task, user);
                fileName = FileLoadManager.uploadFile(request, idTask);
                if (!fileName.isEmpty()){
                    taskDAO.setFileName(fileName, idTask);
                }


            forward(Constants.JUMP_UPDATE, request, response);
        } catch (DaoException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            forwardError(Constants.JUMP_INDEX, request, response);
        }

    }

    private Date getDate(String sectionStr) {
        Section section = Section.valueOf(sectionStr.toUpperCase());
        Date date = null;

        switch (section) {
            case TODAY:
                date = new Date(System.currentTimeMillis());
                break;
            case TOMORROW:
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DATE, 1);
                date = new Date(calendar.getTime().getTime());
                break;
        }

        return date;

    }

}