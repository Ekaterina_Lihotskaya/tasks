package by.gsu.epamlab.controllers.taskControllers;

import by.gsu.epamlab.constants.Constants;
import by.gsu.epamlab.constants.ConstantsJSP;
import by.gsu.epamlab.ifaces.BaseController;
import by.gsu.epamlab.ifaces.ITaskDAO;
import by.gsu.epamlab.model.beans.User;
import by.gsu.epamlab.model.enums.Action;
import by.gsu.epamlab.model.factories.TaskFactory;
import by.gsu.epamlab.model.exceptions.DaoException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ActionsOnTaskController extends BaseController {
    private static final Logger LOGGER = Logger.getLogger(ActionsOnTaskController.class.getName());

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        try {
            User user = (User) session.getAttribute(Constants.KEY_USER);
            String actionStr = request.getParameter(ConstantsJSP.KEY_ACTION);
            Action action = Action.valueOf(actionStr.toUpperCase());
            String [] idTasks = request.getParameterValues(ConstantsJSP.KEY_CHEСKBOX);
            ITaskDAO taskDAO = TaskFactory.getClassFromFactory();

            if (action == Action.REMOVE_ALL){
                taskDAO.removeAll(user);
            }
            else {
                if (idTasks != null) {
                    taskDAO.actionByIdTask(action, idTasks);
                }
            }

            forward(Constants.JUMP_UPDATE, request, response);
        } catch (DaoException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            forwardError(Constants.JUMP_INDEX, request, response);
        }

    }


}
