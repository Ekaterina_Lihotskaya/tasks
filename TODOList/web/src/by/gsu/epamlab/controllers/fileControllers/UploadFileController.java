package by.gsu.epamlab.controllers.fileControllers;

import by.gsu.epamlab.constants.Constants;
import by.gsu.epamlab.constants.ConstantsJSP;

import by.gsu.epamlab.ifaces.BaseController;
import by.gsu.epamlab.ifaces.ITaskDAO;
import by.gsu.epamlab.model.exceptions.DaoException;
import by.gsu.epamlab.model.factories.TaskFactory;
import by.gsu.epamlab.utils.FileLoadManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

@MultipartConfig
public class UploadFileController extends BaseController {
    private static final Logger LOGGER = Logger.getLogger(UploadFileController.class.getName());
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try{
            String idTaskStr = request.getParameter(ConstantsJSP.KEY_ID_TASK);
            int idTask = Integer.parseInt(idTaskStr);
            String fileName = FileLoadManager.uploadFile(request, idTask);
            ITaskDAO taskDAO = TaskFactory.getClassFromFactory();
            taskDAO.setFileName(fileName, idTask);
            forward(Constants.JUMP_UPDATE, request, response);
        } catch (DaoException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            forwardError(Constants.JUMP_INDEX, request, response);
        }
    }
}
