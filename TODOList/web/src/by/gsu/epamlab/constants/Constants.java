package by.gsu.epamlab.constants;

public final class Constants {

	public static final String KEY_USER = "user";
    public static final String JUMP_INDEX = "/index.jsp";
    public static final String JUMP_LOGIN = "/login.jsp";
    public static final String JUMP_REGISTRATE = "/registrate.jsp";
    public static final String JUMP_UPDATE_JSP = "/update.jsp";
    public static final String JUMP_UPDATE = "/update";
    public static final String KEY_ERROR_MESSAGE = "errorMessage";
    public static final String KEY_USER_DAO = "user_dao";
    public static final String KEY_TASK_DAO = "task_dao";
    public static final String ERROR_PASSWORD_LOGIN = "Wrong password or login";
    public static final String ERROR_USER_EXIST = "A user with this login exists";
    public static final String ERROR_DAO = "Error DAO";
    public static final String EMPTY = "";
    public static final String OK = "Ok";
    public static final String ERROR_NULL = "Null field. Please fill all fields";
    public static final String ERROR_EMPTY = "Empty field. Please fill all fields";
    public static final String DRIVER_NAME = "org.gjt.mm.mysql.Driver";
    public static final String DB_URL = "jdbc:mysql://localhost/todo";
    public static final String DB_USER_NAME = "kat";
    public static final String DB_PASSWORD = "1111";
    public static final String DEFAULT_SECTION = "Today";
	public static final String FILE_PATH = "G:\\123\\";

}
