package by.gsu.epamlab.constants;


public class ConstantsJSP {
    public static final String KEY_LOGIN = "login";
    public static final String KEY_PASSWORD = "password";
	public static final String KEY_NAME = "name";
    public static final String KEY_PHONE = "phone";
    public static final String KEY_E_MAIL = "eMail";
    public static final String KEY_SECTION = "section";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_DATE = "date";    
    public static final String KEY_FILE_NAME = "file name";

	public static final String KEY_CHEСKBOX = "idTaskCheckbox";
	public static final String KEY_ID_TASK = "idTask";
	public static final String KEY_ACTION = "action";
}
