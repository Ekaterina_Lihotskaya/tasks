package by.gsu.epamlab.model.factories;

import by.gsu.epamlab.ifaces.IUserDAO;

import by.gsu.epamlab.model.impl.UserImplDB;

public class UserFactory {
    private static IUserDAO userDAO;
    public static void setUserDAO(String strDAO) {
        userDAO = "DB".equals(strDAO) ?
                new UserImplDB() : null;
    }

    public static IUserDAO getClassFromFactory() {
        return userDAO;
    }
}
