package by.gsu.epamlab.model.factories;

import by.gsu.epamlab.ifaces.ITaskDAO;
import by.gsu.epamlab.model.impl.TaskImplDB;


public class TaskFactory {
    private static ITaskDAO taskDAO;

    public static void setTaskDAO(String strDAO){
        taskDAO = "DB".equals(strDAO) ?
                new TaskImplDB() : null;

    }

    public static ITaskDAO getClassFromFactory() {
        return taskDAO;
    }
}