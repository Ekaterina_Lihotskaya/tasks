package by.gsu.epamlab.model.enums;

import by.gsu.epamlab.model.constants.ConstantsSQL;

public enum Action {
    ADD(ConstantsSQL.ADD_TASK),
    DELETE(ConstantsSQL.DELETE_TASK),
    REMOVE(ConstantsSQL.REMOVE_TASK_FOR_RECYCLE_BIN),
    REMOVE_ALL(ConstantsSQL.REMOVE_ALL_TASKS),
    FIXED(ConstantsSQL.FIX_TASK),
    RECOVER(ConstantsSQL.RECOVER_TASK);

    private final String query;

    Action(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }
}
