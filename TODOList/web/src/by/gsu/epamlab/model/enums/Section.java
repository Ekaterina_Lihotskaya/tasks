package by.gsu.epamlab.model.enums;

import by.gsu.epamlab.model.beans.User;
import by.gsu.epamlab.model.constants.ConstantsSQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public enum Section {
    TODAY("active", ConstantsSQL.SELECT_TASKS_TODAY),
    TOMORROW("active", ConstantsSQL.SELECT_TASKS_TOMORROW),
    SOMEDAY("active", ConstantsSQL.SELECT_TASKS_SOMEDAY),
    FIXED("fixed", ConstantsSQL.SELECT_TASKS_FIXED_RECYCLE),
    RECYCLE_BIN("recycle bin", ConstantsSQL.SELECT_TASKS_FIXED_RECYCLE);

    private final String type;
    private final String query;


    Section(String type, String query) {
        this.type = type;
        this.query = query;
    }

    public String getType() {
        return type;
    }

    public String getQuery() {
        return query;
    }

    public PreparedStatement prepareQuery(Connection connection, User user) throws SQLException {

        PreparedStatement preparedStatement = connection.prepareStatement(getQuery());
        final int INDEX_LOGIN = 1;
        final int INDEX_SECTION = 2;
        preparedStatement.setString(INDEX_LOGIN, user.getLogin());
        preparedStatement.setString(INDEX_SECTION, getType());
        return preparedStatement;
    }
}