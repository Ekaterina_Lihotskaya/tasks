package by.gsu.epamlab.model.constants;

public class ConstantsSQL {
	public final static String INSERT_USER = "INSERT INTO users(name, login, password, phone, eMail) VALUES (?, ?, ?, ?, ?)";
	public final static String SELECT_USER = "SELECT * from users where login = ? and password=?";
	public final static String SELECT_LOGIN_USER = "SELECT login FROM users WHERE login=?";
	public final static String SELECT_TASKS_HEAD = "SELECT idTask, description, date, fileName FROM tasks JOIN users ON users.idUser = tasks.userId WHERE login=? AND type = ?";

	public final static String SELECT_TASKS_TODAY = SELECT_TASKS_HEAD + "AND date <= CURDATE()";
	public final static String SELECT_TASKS_TOMORROW = SELECT_TASKS_HEAD + "AND date = ADDDATE(CURDATE(), INTERVAL 1 DAY)";
	public final static String SELECT_TASKS_SOMEDAY = SELECT_TASKS_HEAD + "AND date > ADDDATE(CURDATE(), INTERVAL 1 DAY)";
	public final static String SELECT_TASKS_FIXED_RECYCLE = SELECT_TASKS_HEAD;

	public final static String ADD_TASK = "INSERT INTO tasks(idTask, description, date, userId, type) VALUES (Null, ?, ?, (SELECT idUser FROM users WHERE login = ?),'active')";
	public final static String DELETE_TASK = "UPDATE tasks SET type= 'recycle bin'  WHERE idTask = ?";
	public final static String REMOVE_TASK_FOR_RECYCLE_BIN = "DELETE FROM tasks WHERE idTask = ?";
	public final static String REMOVE_ALL_TASKS = "DELETE FROM tasks where idTask = (SELECT idTask FROM users WHERE login = ?) AND type = 'recycle bin' ";

	public final static String FIX_TASK = "UPDATE tasks SET type='fixed' WHERE idTask = ?";
	public final static String RECOVER_TASK = "UPDATE tasks SET type= 'active'  WHERE idTask = ?";


	public final static String DELETE_FILE = "UPDATE tasks SET fileName = ''  WHERE idTask = ?";
	public final static String SET_FILE_NAME = "UPDATE tasks SET fileName = ?  WHERE idTask = ?";
	public final static String GET_FILE_NAME = "SELECT fileName FROM tasks WHERE idTask = ?";

	public final static String SELECT_ID_TASK = "SELECT idTask FROM tasks JOIN users ON users.idUser = tasks.userId WHERE description = ? AND date = ? AND login=?  ";

}
