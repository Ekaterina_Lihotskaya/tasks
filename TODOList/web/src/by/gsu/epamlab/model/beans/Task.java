package by.gsu.epamlab.model.beans;


import java.sql.Date;

public class Task {
    private int idTask;
    private String description;
    private Date date;
    private String fileName;

    public Task() {
    }

    public Task(String description, Date date) {
        this.description = description;
        this.date = date;

    }

    public Task(String description, Date date, String fileName) {
        this.description = description;
        this.date = date;
        this.fileName = fileName;
    }

    public Task(int idTask, String description, Date date, String fileName) {
        this.idTask = idTask;
        this.description = description;
        this.date = date;
        this.fileName = fileName;
    }

    public int getIdTask() {
        return idTask;
    }

    public void setIdTask(int idTask) {
        this.idTask = idTask;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}