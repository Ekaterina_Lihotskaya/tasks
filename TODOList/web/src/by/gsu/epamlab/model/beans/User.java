package by.gsu.epamlab.model.beans;

public class User {
    private String name;
    private String login;
    private String password;
    private String phone;
    private String eMail;

    public User() {
        super();
    }

    public User(String name, String login, String password, String phone, String eMail) {
        this.name = name;
        this.login = login;
        this.password = password;
        this.phone = phone;
        this.eMail = eMail;
    }

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }
}
