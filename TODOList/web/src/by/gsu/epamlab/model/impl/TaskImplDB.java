package by.gsu.epamlab.model.impl;

import by.gsu.epamlab.ifaces.ITaskDAO;
import by.gsu.epamlab.model.beans.Task;
import by.gsu.epamlab.model.beans.User;
import by.gsu.epamlab.model.constants.ConstantsSQL;
import by.gsu.epamlab.model.db.ConnectionDB;
import by.gsu.epamlab.model.enums.Action;
import by.gsu.epamlab.model.enums.Section;
import by.gsu.epamlab.model.exceptions.DaoException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import java.util.logging.Level;
import java.util.logging.Logger;

public class TaskImplDB implements ITaskDAO {
    private static final Logger LOGGER = Logger.getLogger(TaskImplDB.class.getName());

    @Override
    public List<Task> getTasks(User user, String section) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Task> tasks = new ArrayList<Task>();

        try {
            connection = ConnectionDB.createConnection();
            preparedStatement = Section.valueOf(section.toUpperCase()).prepareQuery(connection, user);
            resultSet = preparedStatement.executeQuery();

            final int INDEX_ID = 1;
            final int INDEX_DESCRIPTION = 2;
            final int INDEX_DATE = 3;
            final int INDEX_FILE_NAME = 4;

            while (resultSet.next()) {
                int idTask = resultSet.getInt(INDEX_ID);
                String description = resultSet.getString(INDEX_DESCRIPTION);
                Date date = resultSet.getDate(INDEX_DATE);
                String fileName = resultSet.getString(INDEX_FILE_NAME);
                tasks.add(new Task(idTask, description, date, fileName));
            }
            return tasks;

        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            return new ArrayList<Task>();

        } finally {
            ConnectionDB.closeResultSet(resultSet);
            ConnectionDB.closeStatement(preparedStatement);
            ConnectionDB.closeConnection(connection);
        }
    }

    @Override
    public void actionByIdTask(Action action, String[] idTasks) throws DaoException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        final int INDEX_ID_TASK = 1;

        try {
            connection = ConnectionDB.createConnection();
            preparedStatement = connection.prepareStatement(action.getQuery());
            for (String idTaskStr : idTasks) {
                int idTask = Integer.parseInt(idTaskStr);
                preparedStatement.setInt(INDEX_ID_TASK, idTask);
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException();

        } finally {
            ConnectionDB.closeStatement(preparedStatement);
            ConnectionDB.closeConnection(connection);
        }
    }


    @Override
    public int add(Task task, User user) throws DaoException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        final int INDEX_DESCRIPTION = 1;
        final int INDEX_DATE = 2;
        final int INDEX_lOGIN = 3;
        final int INDEX_ID_TASK = 1;

        try {
            connection = ConnectionDB.createConnection();
            preparedStatement = connection.prepareStatement(Action.ADD.getQuery());
            preparedStatement.setString(INDEX_DESCRIPTION, task.getDescription());
            preparedStatement.setDate(INDEX_DATE, task.getDate());
            preparedStatement.setString(INDEX_lOGIN, user.getLogin());
            preparedStatement.executeUpdate();

            preparedStatement = connection.prepareStatement(ConstantsSQL.SELECT_ID_TASK);
            preparedStatement.setString(INDEX_DESCRIPTION, task.getDescription());
            preparedStatement.setDate(INDEX_DATE, task.getDate());
            preparedStatement.setString(INDEX_lOGIN, user.getLogin());
            resultSet = preparedStatement.executeQuery();
            int idTask = 0;
            while (resultSet.next()) {
                idTask = resultSet.getInt(INDEX_ID_TASK);
            }
            System.out.println(idTask);
            return idTask;


        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException();

        } finally {
            ConnectionDB.closeStatement(preparedStatement);
            ConnectionDB.closeConnection(connection);
        }
    }


    @Override
    public void removeAll(User user) throws DaoException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        final int INDEX_ID_LOGIN = 1;

        try {
            connection = ConnectionDB.createConnection();
            preparedStatement = connection.prepareStatement(Action.REMOVE_ALL.getQuery());
            preparedStatement.setString(INDEX_ID_LOGIN, user.getLogin());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException();

        } finally {
            ConnectionDB.closeStatement(preparedStatement);
            ConnectionDB.closeConnection(connection);
        }
    }

    @Override
    public void deleteFile(int idTask) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        final int INDEX_ID_TASK = 1;

        try {
            connection = ConnectionDB.createConnection();
            preparedStatement = connection.prepareStatement(ConstantsSQL.DELETE_FILE);
            preparedStatement.setInt(INDEX_ID_TASK, idTask);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException();

        } finally {
            ConnectionDB.closeStatement(preparedStatement);
            ConnectionDB.closeConnection(connection);
        }
    }

    @Override
    public String getFileName(int idTask) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        final int INDEX_ID_TASK = 1;
        final int INDEX_ID_FILE_NAME = 1;
        String fileName = null;

        try {
            connection = ConnectionDB.createConnection();
            preparedStatement = connection.prepareStatement(ConstantsSQL.GET_FILE_NAME);
            preparedStatement.setInt(INDEX_ID_TASK, idTask);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                fileName = resultSet.getString(INDEX_ID_FILE_NAME);
            }

            return fileName;
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException();

        } finally {
            ConnectionDB.closeStatement(preparedStatement);
            ConnectionDB.closeConnection(connection);
        }

    }


    @Override
    public void setFileName(String filename, int idTask) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        final int INDEX_FILE_NAME = 1;
        final int INDEX_ID_TASK = 2;


        try {
            connection = ConnectionDB.createConnection();
            preparedStatement = connection.prepareStatement(ConstantsSQL.SET_FILE_NAME);
            preparedStatement.setString(INDEX_FILE_NAME, filename);
            preparedStatement.setInt(INDEX_ID_TASK, idTask);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException();

        } finally {
            ConnectionDB.closeStatement(preparedStatement);
            ConnectionDB.closeConnection(connection);
        }

    }


}
