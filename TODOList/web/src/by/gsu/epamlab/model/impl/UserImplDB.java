package by.gsu.epamlab.model.impl;

import by.gsu.epamlab.ifaces.IUserDAO;
import by.gsu.epamlab.model.constants.ConstantsSQL;
import by.gsu.epamlab.model.db.ConnectionDB;
import by.gsu.epamlab.model.exceptions.DaoException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserImplDB implements IUserDAO {

    private static final Logger LOGGER = Logger.getLogger(UserImplDB.class.getName());
    private final int INDEX_LOGIN_SELECT = 1;

    @Override
    public boolean getUser(String login, String password) throws DaoException {
        final int INDEX_PASSWORD_SELECT = 2;
        Connection connection = null;
        PreparedStatement prepareStatement = null;
        ResultSet resultSet = null;

        try {
            connection = ConnectionDB.createConnection();
            prepareStatement = connection.prepareStatement(ConstantsSQL.SELECT_USER);
            prepareStatement.setString(INDEX_LOGIN_SELECT, login);
            prepareStatement.setString(INDEX_PASSWORD_SELECT, password);
            resultSet = prepareStatement.executeQuery();
            return resultSet.next();

        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException();
        } finally {
            ConnectionDB.closeResultSet(resultSet);
            ConnectionDB.closeStatement(prepareStatement);
            ConnectionDB.closeConnection(connection);
        }

    }

    @Override
    public boolean addUser(String name, String login, String password, String phone, String eMail) throws DaoException {
        final int INDEX_NAME = 1;
        final int INDEX_LOGIN = 2;
        final int INDEX_PASSWORD = 3;
        final int INDEX_PHONE = 4;
        final int INDEX_E_MAIL = 5;

        Connection connection = null;
        PreparedStatement psSelectLogin = null;
        PreparedStatement psInsertUser = null;
        ResultSet resultSet = null;

        try {
            connection = ConnectionDB.createConnection();
            psSelectLogin = connection.prepareStatement(ConstantsSQL.SELECT_LOGIN_USER);
            psSelectLogin.setString(INDEX_LOGIN_SELECT, login);
            psInsertUser = connection.prepareStatement(ConstantsSQL.INSERT_USER);

            synchronized (UserImplDB.class) {
                resultSet = psSelectLogin.executeQuery();
                if (!resultSet.next()) {
                    psInsertUser.setString(INDEX_NAME, name);
                    psInsertUser.setString(INDEX_LOGIN, login);
                    psInsertUser.setString(INDEX_PASSWORD, password);
                    psInsertUser.setString(INDEX_PHONE, phone);
                    psInsertUser.setString(INDEX_E_MAIL, eMail);
                    psInsertUser.executeUpdate();
                    return true;
                }
            }
            return false;
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException();

        } finally {
            ConnectionDB.closeResultSet(resultSet);
            ConnectionDB.closeStatement(psSelectLogin);
            ConnectionDB.closeStatement(psInsertUser);
            ConnectionDB.closeConnection(connection);
        }

    }
}

