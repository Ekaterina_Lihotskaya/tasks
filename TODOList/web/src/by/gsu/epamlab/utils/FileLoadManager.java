package by.gsu.epamlab.utils;

import by.gsu.epamlab.constants.Constants;
import by.gsu.epamlab.constants.ConstantsJSP;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;


public class FileLoadManager {
    public static String uploadFile(HttpServletRequest request, int idTask) throws IOException, ServletException{
        String savePath = Constants.FILE_PATH;
        Part filePart = request.getPart(ConstantsJSP.KEY_FILE_NAME);
        String fileName = filePart.getSubmittedFileName();
        if (!fileName.isEmpty()) {
            InputStream fileContent = filePart.getInputStream();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte buf[] = new byte[8192];
            int qt = 0;
            while ((qt = fileContent.read(buf)) != -1) {
                baos.write(buf, 0, qt);
            }

            try {
                fileName = "Task " + idTask + fileName;
                RandomAccessFile f = new RandomAccessFile(
                        savePath + fileName, "rw");
                byte[] bytes = baos.toByteArray();
                f.write(bytes);
                f.close();

            } catch (Exception e) {
                return "";
            }


        }
        return fileName;

    }

    public static void downloadFile (HttpServletResponse response, String filepath, String filename) throws IOException{
        response.setContentType(
                "APPLICATION/OCTET-STREAM");
        response.setHeader("Content-Disposition",
                "attachment; filename=\"" + filename + "\"");
        FileInputStream fileInputStream =
                new FileInputStream(filepath + filename);
        ServletOutputStream outputStream = response.getOutputStream();
        int i;
        while ((i = fileInputStream.read()) != -1) {
            outputStream.write(i);
        }
        fileInputStream.close();
        outputStream.close();


    }
}