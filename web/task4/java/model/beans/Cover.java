package model.beans;

/**
 * Description of the instance of the cover
 */
public class Cover {
    private int idCover;
    private int idBook;
    private String fileName;

    /**
     * Initializes a newly created object
     */
    public Cover() {
    }

    /**
     * Initializes a newly created object
     * @param idBook idBook
     * @param fileName name of file with cover
     */
    public Cover(final int idBook, final String fileName) {
        this.idBook = idBook;
        this.fileName = fileName;
    }

    /**
     * get IdBook
     * @return IdBook
     */
    public int getIdBook() {
        return idBook;
    }

    /**
     * set IdBook
     * @param idBook IdBook
     */
    public void setIdBook(final int idBook) {
        this.idBook = idBook;
    }

    /**
     * get file name
     * @return file name
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * set file name
     * @param fileName file name
     */
    public void setFileName(final String fileName) {
        this.fileName = fileName;
    }

    /**
     * get IdCover
     * @return IdCover
     */
    public int getIdCover() {
        return idCover;
    }

    /**
     * set IdCover
     * @param idCover IdCover
     */
    public void setIdCover(final int idCover) {
        this.idCover = idCover;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cover cover = (Cover) o;

        if (idBook != cover.idBook) return false;
        return fileName != null ? fileName.equals(cover.fileName) : cover.fileName == null;
    }

    @Override
    public int hashCode() {
        int result = idBook;
        result = 31 * result + (fileName != null ? fileName.hashCode() : 0);
        return result;
    }
}