package model.exceptions;

/**
 * description of the exception class encountered in the DAO layer
 */
public class DaoException extends RuntimeException {
    /**
     * Initializes a newly created object
     */
    public DaoException() {}

    /**
     * Initializes a newly created object
     * @param message message
     */
    public DaoException(final String message) {
        super(message);
    }

    /**
     * message
     * @param cause cause
     */
    public DaoException(final Throwable cause) {
    	super(cause);
    }

    /**
     * cause
     * @param message message
     * @param cause cause of exception
     */
    public DaoException(final String message, final Throwable cause) {
    	super(message, cause);
    }
}
