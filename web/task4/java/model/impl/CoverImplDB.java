package model.impl;

import ifaces.ICoverDAO;
import model.beans.Cover;
import model.db.ConnectionDB;
import model.exceptions.DaoException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class allows you to work with a database of books
 */
public class CoverImplDB implements ICoverDAO {
    private static final Logger LOGGER = Logger.getLogger(CoverImplDB.class.getName());
    private static final String ADD_COVER = "INSERT INTO cover(idCover, idBook, fileName) VALUES (Null, ?, ?)";


    @Override
    public void addCover(final Cover cover) {
        final int INDEX_ID_BOOK_ADD = 1;
        final int INDEX_FILE_NAME_ADD = 2;
        try (Connection connection = ConnectionDB.createConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ADD_COVER)) {

            preparedStatement.setInt(INDEX_ID_BOOK_ADD, cover.getIdBook());
            preparedStatement.setString(INDEX_FILE_NAME_ADD, cover.getFileName());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException(e.getMessage());
        }
    }
}
