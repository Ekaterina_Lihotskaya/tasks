package servlets;

import com.sun.org.apache.xpath.internal.operations.Number;
import ifaces.ICoverDAO;
import model.beans.Cover;
import model.exceptions.DaoException;
import model.impl.CoverImplDB;
import service.FileLoadManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class allows you to upload cover for book and add his path to DB
 */

@MultipartConfig
public class UploadBookCoverImagesServlet extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(UploadBookCoverImagesServlet.class.getName());
    private static final String KEY_ID_BOOK = "idBook";
    private ICoverDAO coverDAO = new CoverImplDB();
    private static final String KEY_MESSAGE_DO_ACTION = "messageAction";
    private static final String KEY_FILE_NAME = "file name";
    private static final String JUMP_PAGE = "/upload.jsp";
    private final FileLoadManager fileLoadManager = new FileLoadManager();

    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        String message;
        try {
            Part filePart = request.getPart(KEY_FILE_NAME);
            String typeFile = getServletConfig().getInitParameter("typeFile");
            if (typeFile.equals(filePart.getContentType())) {
                String fileName = fileLoadManager.uploadFile(filePart);
                int idBook = Integer.valueOf(request.getParameter(KEY_ID_BOOK));
                Cover cover = new Cover();
                cover.setIdBook(idBook);
                cover.setFileName(fileName);
                coverDAO.addCover(cover);
                message = "You upload cover for the book with id = " + idBook;
                request.setAttribute(KEY_MESSAGE_DO_ACTION, message);
                LOGGER.info(message);
            } else {
                message = "You can upload only .jpg/.jpeg files";
                LOGGER.info(message);
                request.setAttribute(KEY_MESSAGE_DO_ACTION, message);
            }
        } catch (DaoException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            request.setAttribute(KEY_MESSAGE_DO_ACTION, "You don't upload cover for the book");
        } catch (NumberFormatException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            request.setAttribute(KEY_MESSAGE_DO_ACTION, "You input wrong idBook");
        }

        RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(JUMP_PAGE);
        requestDispatcher.forward(request, response);
    }
}
