package filters;

import servlets.UploadBookCoverImagesServlet;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LogUploadCoverFilter implements Filter {
    private static final Logger LOGGER = Logger.getLogger(LogUploadCoverFilter.class.getName());
    private FilterConfig filterConfig = null;

    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse,
                         final FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String servletPath = request.getServletPath();
        LOGGER.info("There was a request to download the cover " + new Date());
        filterChain.doFilter(request, servletResponse);
    }
    @Override
    public void destroy() {
    }
}

