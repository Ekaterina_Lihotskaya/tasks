package ifaces;

import model.beans.Cover;
/**
 * Contract for a DAO Book
 */
public interface ICoverDAO {
    /**
     * Add a book cover
     * @param cover the cover is added in DB
     */
    void addCover(Cover cover);
}
