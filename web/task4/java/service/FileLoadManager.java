package service;

import javax.servlet.ServletException;
import javax.servlet.http.Part;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class allows you to work with file
 */
public class FileLoadManager {
    private static final String FILE_PATH = "G:\\123\\";
    private static final Logger LOGGER = Logger.getLogger(FileLoadManager.class.getName());

    /**
     * upload File
     * @param filePart file Part
     * @return path to upload file
     * @throws IOException available exception
     * @throws ServletException available exception
     */

    public String uploadFile(final Part filePart) throws IOException, ServletException {
        String fileName = filePart.getSubmittedFileName();
        try (InputStream fileInputStream = filePart.getInputStream()) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buf = new byte[8192];
            int qt = 0;
            while ((qt = fileInputStream.read(buf)) != -1) {
                baos.write(buf, 0, qt);
            }
            fileName = FILE_PATH + fileName;
            RandomAccessFile f = new RandomAccessFile(
                    fileName, "rw");
            byte[] bytes = baos.toByteArray();
            f.write(bytes);
            f.close();

        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
        }
        return fileName;
    }
}
