<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="utils" uri="/WEB-INF/utils.tld" %>

<html>
<head>
    <title>Custom Tags</title>
</head>
<body>

<h1>TAG listmapping </h1>
<utils:listmapping>
    ${url} - ${servlet} <br>
</utils:listmapping>

<%
    request.setAttribute("url_to_test", "/retrieveBookServlet");
%>

<br><br>
<h1> TAG resolveurl </h1>
<utils:resolveurl url ="${url_to_test}">
    ${url} - ${servlet}
</utils:resolveurl>

</body>
</html>
