<%@ page import="by.training.constants.ConstantsJSP" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Book</title>
</head>

<body>
<h1> Hello! I am simple web application. <br>
    I want to help you retrieve books from database.<br>
    Now I can save and update books.<br>
    Write the author's name and his book and then press the botton<br>
</h1>

<c:if test="${not empty messageAction}">
    <c:out value="${messageAction}"/>
    <hr>
</c:if>

<h1> Retrieve book </h1>
<form name="chooseBook" method="get" action="/retrieveBookServlet">
    Author:<br>
    <input type="text" name=<%= ConstantsJSP.KEY_AUTHOR %> value=""><br><br>
    Book:<br>
    <input type="text" name=<%= ConstantsJSP.KEY_TITLE_BOOK %> value=""><br><br>
    <input type="submit"  value="Retrieve">
</form>
<hr>

<h1> Add book </h1>
<form name="chooseBook" method="post" action="/addBookServlet">
    Author:<br>
    <input type="text" name=<%= ConstantsJSP.KEY_AUTHOR %> value=""><br><br>
    Book:<br>
    <input type="text" name=<%= ConstantsJSP.KEY_TITLE_BOOK %> value=""><br><br>
    Description:<br>
    <input type="text" name=<%= ConstantsJSP.KEY_DESCRIPTION_BOOK %> value=""><br><br>
    <input type="submit"  value="Add">
</form>
<hr>

<h1> Update book </h1>
<h1> Enter the book you want to update </h1>
<form name="chooseBook" method="post" action="/updateBookServlet">
    Author:<br>
    <input type="text" name=<%= ConstantsJSP.KEY_AUTHOR %> value=""><br><br>
    Book:<br>
    <input type="text" name=<%= ConstantsJSP.KEY_TITLE_BOOK %> value=""><br><br>


<h1> Enter the required changes </h1>
    Author:<br>
    <input type="text" name=<%= ConstantsJSP.KEY_AUTHOR_UPDATE %> value=""><br><br>
    Book:<br>
    <input type="text" name=<%= ConstantsJSP.KEY_TITLE_BOOK_UPDATE %> value=""><br><br>
    Description:<br>
    <input type="text" name=<%= ConstantsJSP.KEY_DESCRIPTION_BOOK_UPDATE %> value=""><br><br>
    <input type="submit" value="Update">
</form>



</body>

</html>