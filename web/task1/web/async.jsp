<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Async functionality</title>
</head>

<body>

<h1>Hello! YOU come to some special page,<br>
    some long async process was emulated.<br>
    Result of this process will be added to request attributes, <br>
    so it could be viewed on the page <br>
    <form name="chooseBook" method="get" action="/asyncServlet">

        <input type="submit" value="Go">
    </form>

</h1>

<c:if test="${not empty First}">
    First =
    <c:out value="${First}"/>
    <hr>
</c:if><br>

<c:if test="${not empty Second}">
    Second =
    <c:out value="${Second}"/>
    <hr>
</c:if>

</body>

</html>