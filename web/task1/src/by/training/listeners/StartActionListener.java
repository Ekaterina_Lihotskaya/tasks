package by.training.listeners;

import by.training.model.factories.BookFactory;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRegistration;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Output in the console basic information about application
 */
public class StartActionListener implements ServletContextListener {
    private static final Logger LOGGER = Logger.getLogger(StartActionListener.class.getName());
    private static final String KEY_BOOK_DAO = "book_dao";

    @Override
    public void contextInitialized(final ServletContextEvent servletContextEvent) {
        ServletContext servletContext = servletContextEvent.getServletContext();
        LOGGER.info("\nApplication name is " + servletContext.getServletContextName());
        Map<String, ? extends ServletRegistration> listServlets = servletContext.getServletRegistrations();

        LOGGER.info("\nAvailable servlets:");
        for (Map.Entry<String, ? extends ServletRegistration> pair : listServlets.entrySet()) {
            System.out.println(pair.getKey());
        }
        String strBookDAO = servletContext.getInitParameter(KEY_BOOK_DAO);
        BookFactory.setBookDAO(strBookDAO);
    }

    @Override
    public void contextDestroyed(final ServletContextEvent servletContextEvent) {

    }
}