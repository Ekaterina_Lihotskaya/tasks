package by.training.listeners;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.Calendar;
import java.util.logging.Logger;

/**
 * SessionListner logs information when session was activated and deactivated.
 */
@WebListener
public class SessionListner implements HttpSessionListener {
    private static final Logger LOGGER = Logger.getLogger(SessionListner.class.getName());

    @Override
    public void sessionCreated(final HttpSessionEvent httpSessionEvent) {
        LOGGER.info("Session id = " + httpSessionEvent.getSession().getId() + "created at " + Calendar.getInstance());
    }

    @Override
    public void sessionDestroyed(final HttpSessionEvent httpSessionEvent) {
        LOGGER.info("Session id = " + httpSessionEvent.getSession().getId() + "destroyed at " + Calendar.getInstance());

    }
}