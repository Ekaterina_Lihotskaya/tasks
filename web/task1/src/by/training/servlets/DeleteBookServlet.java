package by.training.servlets;

import by.training.constants.ConstantsJSP;
import by.training.ifaces.BaseServlet;
import by.training.ifaces.IBookDAO;
import by.training.model.factories.BookFactory;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Delete existing books from system
 */
@ServletSecurity(@HttpConstraint(rolesAllowed = "user_management"))
public class DeleteBookServlet extends BaseServlet {

    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter(ConstantsJSP.KEY_ID_BOOK);

        checkValueParam(request, response, id);
        IBookDAO taskDAO = BookFactory.getClassFromFactory();

        try {
            Integer idBook = Integer.parseInt(id);
            taskDAO.deleteBook(idBook);
            messageDoAction = "You delete the book with id = " + id;
        } catch (Exception e) {
            messageDoAction = "You cannot delete the book with id = " + id;
        }
        forwardError(messageDoAction, request, response);
    }
}
