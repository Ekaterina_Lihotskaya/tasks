package by.training.servlets;

import by.training.ifaces.BaseServlet;
import by.training.model.impl.BookImplDB;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * AsyncServlet provides async functionality
 */
@WebServlet(urlPatterns = {"/asyncServlet"}, asyncSupported = true)
public class AsyncServlet extends BaseServlet {

    private static final Logger LOGGER = Logger.getLogger(BookImplDB.class.getName());

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
       AsyncContext asyncContext = request.startAsync();

        asyncContext.start(() -> {
            try {
                Thread.sleep(1000);
                asyncContext.getRequest().setAttribute("First", "First attribute calculated in AsyncServlet at "
                        + new SimpleDateFormat(PATTERN_DATE).format(Calendar.getInstance().getTime()));
                asyncContext.dispatch("/additionalServlet");

            } catch (InterruptedException e) {
                LOGGER.log(Level.SEVERE, e.toString(), e);
            }
        });
    }
}