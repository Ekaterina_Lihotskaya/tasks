package by.training.servlets;

import by.training.ifaces.BaseServlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Additional functionality: sets the query attribute
 */
@WebServlet(urlPatterns = {"/additionalServlet"})
public class AdditionalServletForAsync extends BaseServlet {
    private static final String JUMP_ASYNC = "/async.jsp";

    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("Second", "Second attribute calculated in AdditionalServletForAsync at "
                + new SimpleDateFormat(PATTERN_DATE).format(Calendar.getInstance().getTime()));
        forward(JUMP_ASYNC, request, response);
    }
}
