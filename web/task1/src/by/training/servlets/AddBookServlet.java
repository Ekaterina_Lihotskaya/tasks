package by.training.servlets;

import by.training.ifaces.BaseServlet;
import by.training.ifaces.IBookDAO;
import by.training.model.beans.Book;
import by.training.model.factories.BookFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The controller processes the request to add books
 */
public class AddBookServlet extends BaseServlet {
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {

        Book book = getBookFromRequest(request, response);
        IBookDAO taskDAO = BookFactory.getClassFromFactory();

        try {
            taskDAO.addBook(book);
            messageDoAction = "You add a book " + book.getAuthor() + " - " + book.getTitle();
        } catch (Exception e) {
            messageDoAction = "You cannot add a book " + book.getAuthor() + " - " + book.getTitle()  + " Cause: " + e.getMessage();
        }

        forwardError(messageDoAction, request, response);
    }
}