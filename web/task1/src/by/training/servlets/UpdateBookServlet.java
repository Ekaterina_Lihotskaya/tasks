package by.training.servlets;

import by.training.constants.ConstantsJSP;
import by.training.ifaces.BaseServlet;
import by.training.ifaces.IBookDAO;
import by.training.model.beans.Book;
import by.training.model.factories.BookFactory;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Repeatable functionality for all controllers
 */
public class UpdateBookServlet extends BaseServlet {
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {

        Book currentBook = getBookFromRequest(request, response);
        Book updateBook = getBookFromRequest(request, response, ConstantsJSP.KEY_AUTHOR_UPDATE, ConstantsJSP.KEY_TITLE_BOOK_UPDATE, ConstantsJSP.KEY_DESCRIPTION_BOOK_UPDATE);
        updateBook.setIdBook(currentBook.getIdBook());
        IBookDAO taskDAO = BookFactory.getClassFromFactory();

        try {
            taskDAO.updateBook(updateBook);
            messageDoAction = "You update the book " + currentBook.getAuthor() + " - " + currentBook.getTitle();
        } catch (Exception e) {
            messageDoAction = "You cannot update the book " + currentBook.getAuthor() + " - " + currentBook.getTitle() + " Cause: " + e.getMessage();
        }
        forwardError(messageDoAction, request, response);
    }
}
