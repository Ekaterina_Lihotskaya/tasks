package by.training.servlets;


import by.training.ifaces.BaseServlet;
import by.training.ifaces.IBookDAO;
import by.training.model.beans.Book;
import by.training.model.factories.BookFactory;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;

/**
 * the controller processes the request to get books
 */
public class RetrieveBookServlet extends BaseServlet {
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("resources/ConditionsUnavaliable");
    private static String isUnAvailableServlet = resourceBundle.getString("isUnAvailableServlet");
    private static String timeUnAvailableServlet = resourceBundle.getString("timeUnAvailableServlet");

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {

        Calendar timeUnavailableFrom = new GregorianCalendar(2018, 10, 26, 0, 0);
        Calendar timeUnavailableTo = new GregorianCalendar(2018, 10, 27, 0, 0);
        Calendar currentTime = Calendar.getInstance();

        if (currentTime.after(timeUnavailableFrom) && currentTime.before(timeUnavailableTo)) {
            if ("true".equals(isUnAvailableServlet)) {
                throw new UnavailableException("RetrieveBookServlet will be unavalible until", Integer.parseInt(timeUnAvailableServlet));
            }
        }

        Book book = getBookFromRequest(request, response);
        IBookDAO taskDAO = BookFactory.getClassFromFactory();

        try {
            Book bookfromDB = taskDAO.getBook(book);
            messageDoAction = "You get the book " + bookfromDB;
        } catch (Exception e) {
            messageDoAction = "You don't get the book " + book + " Cause: " + e.getMessage();
        }

        forwardError(messageDoAction, request, response);

    }
}