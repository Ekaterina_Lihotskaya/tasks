package by.training.wrappers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * Wrapper provides XSS protection
 */
public class XSSWrapper extends HttpServletRequestWrapper {
    /**
     * Initializes a newly created object XSSWrapper
     *
     * @param request user request
     */
    public XSSWrapper(final HttpServletRequest request) {
        super(request);
    }

    @Override
    public String getParameter(final String name) {
        String value = super.getParameter(name);
        return replaceWrongCharacters(value);
    }

    @Override
    public String[] getParameterValues(final String name) {
        String[] values = super.getParameterValues(name);
        if (values == null) {
            return null;
        }
        String[] newValues = new String[values.length];
        for (int i = 0; i < values.length; i++) {
            newValues[i] = replaceWrongCharacters(values[i]);
        }
        return newValues;
    }

    @Override
    public String getHeader(final String name) {
        String value = super.getHeader(name);
        return replaceWrongCharacters(value);

    }

    private String replaceWrongCharacters(final String value) {
        String newValue = null;
        if (value != null) {
            newValue = value.replaceAll("<script>.{0,}</script>", "");
        }
        return newValue;
    }
}



