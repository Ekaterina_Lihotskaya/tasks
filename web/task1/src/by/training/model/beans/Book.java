package by.training.model.beans;

/**
 * Description of the essence of the book
 */
public class Book {
    private Integer idBook;
    private String author;
    private String title;
    private String description;

    public Book() {
    }

    /**
     * Initializes a newly created object
     * @param author the author of the book
     * @param title the title of the book
     * @param description the description of the book
     */
    public Book(final String author, final String title, final String description) {
        this.author = author;
        this.title = title;
        this.description = description;
    }

    /**
     * Initializes a newly created object
     * @param idBook the idBook of the book
     * @param author the author of the book
     * @param title the title of the book
     * @param description the description of the book
     */
    public Book(final Integer idBook, final String author, final String title, final String description) {
        this.idBook = idBook;
        this.author = author;
        this.title = title;
        this.description = description;
    }

    /**
     * Get idBook
     * @return idBook
     */
    public Integer getIdBook() {
        return idBook;
    }

    /**
     * Get author
      * @return author of the book
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Get title of the book
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Get description
     * @return description
     */
	public String getDescription() {
        return description;
    }

    /**
     * Set idBook
     * @param idBook idBook
     */
    public void setIdBook(final Integer idBook) {
        this.idBook = idBook;
    }

    /**
     * Set author of the book
     * @param author author of the book
     */
    public void setAuthor(final String author) {
        this.author = author;
    }

    /**
     * Set title of the book
     * @param title of the book
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * Set description
     * @param description of the book
     */
	public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return  idBook + "-" + author + '-' + title + '-' + description;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (idBook != null ? !idBook.equals(book.idBook) : book.idBook != null) return false;
        if (author != null ? !author.equals(book.author) : book.author != null) return false;
        if (title != null ? !title.equals(book.title) : book.title != null) return false;
        return description != null ? description.equals(book.description) : book.description == null;
    }

    @Override
    public int hashCode() {
        int result = idBook != null ? idBook.hashCode() : 0;
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }
}