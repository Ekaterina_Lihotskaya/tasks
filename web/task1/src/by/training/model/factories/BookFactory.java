package by.training.model.factories;

import by.training.ifaces.IBookDAO;
import by.training.model.impl.BookImplDB;

/**
 * Creating a DAO instance
 */
public class BookFactory {
    private static IBookDAO bookDAO;

    /**
     * Set DAO instance
     * @param strBookDAO DAO instance
     */
    public  static void setBookDAO(final String strBookDAO) {
        bookDAO = "DB".equals(strBookDAO) ? new BookImplDB() : null;
    }

    /**
     * Get DAO instance
     * @return DAO instance
     */
    public  static IBookDAO getClassFromFactory() {
        return  bookDAO;
    }

}