package by.training.model.impl;

import by.training.ifaces.IBookDAO;
import by.training.model.beans.Book;
import by.training.model.db.ConnectionDB;
import by.training.model.exceptions.DaoException;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class allows you to work with a database of books
 */
public class BookImplDB implements IBookDAO {
    private static final Logger LOGGER = Logger.getLogger(BookImplDB.class.getName());
    private final static String SELECT_BOOK = "SELECT idBook, author, title, description from book where author = ? and title = ?";
    private final static String ADD_BOOK = "INSERT INTO book(idBook, author, title, description) VALUES (Null, ?, ?, ?)";
    private final static String UPDATE_BOOK = "UPDATE book SET author = ?, title = ?, description = ?  WHERE idBook = ?";
    public final static String DELETE_BOOK = "DELETE FROM book WHERE idBook = ?";

    @Override
    public Book getBook(final Book book) {

        ResultSet resultSet = null;
        final int INDEX_AUTHOR_SELECT = 1;
        final int INDEX_TITLE_BOOK_SELECT = 2;

        final int INDEX_ID_BOOK = 1;
        final int INDEX_DESCRIPTION = 4;

        try (Connection connection = ConnectionDB.createConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BOOK)) {

            preparedStatement.setString(INDEX_AUTHOR_SELECT, book.getAuthor());
            preparedStatement.setString(INDEX_TITLE_BOOK_SELECT, book.getTitle());
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                Integer idBook = resultSet.getInt(INDEX_ID_BOOK);
                String description = resultSet.getString(INDEX_DESCRIPTION);
                return new Book(idBook, book.getAuthor(), book.getTitle(), description);
            }
            throw new DaoException("This book does not exist in DB");
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException(e.getMessage());
        }

    }

    @Override
    public void addBook(final Book book) {

        final int INDEX_AUTHOR_ADD = 1;
        final int INDEX_TITLE_BOOK_ADD = 2;
        final int INDEX_DESCRIPTION_ADD = 3;
        try (Connection connection = ConnectionDB.createConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ADD_BOOK)) {

            preparedStatement.setString(INDEX_AUTHOR_ADD, book.getAuthor());
            preparedStatement.setString(INDEX_TITLE_BOOK_ADD, book.getTitle());
            preparedStatement.setString(INDEX_DESCRIPTION_ADD, book.getDescription());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException(e.getMessage());
        }
    }

    @Override
    public void updateBook(final Book updateBook) {

        final int INDEX_NEW_AUTHOR_UPDATE = 1;
        final int INDEX_NEW_TITLE_BOOK_UPDATE = 2;
        final int INDEX_NEW_DESCRIPTION_UPDATE = 3;
        final int INDEX_ID_BOOK_UPDATE = 4;

        try (Connection connection = ConnectionDB.createConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_BOOK)) {

            preparedStatement.setString(INDEX_NEW_AUTHOR_UPDATE, updateBook.getAuthor());
            preparedStatement.setString(INDEX_NEW_TITLE_BOOK_UPDATE, updateBook.getTitle());
            preparedStatement.setString(INDEX_NEW_DESCRIPTION_UPDATE, updateBook.getDescription());
            preparedStatement.setInt(INDEX_ID_BOOK_UPDATE, updateBook.getIdBook());
            preparedStatement.executeUpdate();

        } catch (SQLException | NullPointerException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException(e.getMessage());
        }
    }

    public void deleteBook(final Integer idBook) {
        final int INDEX_ID_BOOK = 1;
        try (Connection connection = ConnectionDB.createConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE_BOOK)) {
            preparedStatement.setInt(INDEX_ID_BOOK, idBook);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException(e.getMessage());
        }
    }

}
