package by.training.model.db;

import by.training.model.exceptions.DaoException;

import java.sql.*;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class describes connection with DB
 */
public class ConnectionDB {
    private static final Logger LOGGER = Logger.getLogger(ConnectionDB.class.getName());
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("resources/DB");
    private static String driverName = resourceBundle.getString("driverName");
    private static String dbUrl = resourceBundle.getString("dbUrl");
    private static String dbUserName = resourceBundle.getString("dbUserName");
    private static String dbPassword = resourceBundle.getString("dbPassword");

     static {
        try {

            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException("Driver don't find");
        }
    }

    /**
     * Create a database connection
     * @return object connection
     */
    public static Connection createConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);

        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.toString(), e);
            throw new DaoException("Connection isn't");
        }
        return connection;
    }

}
