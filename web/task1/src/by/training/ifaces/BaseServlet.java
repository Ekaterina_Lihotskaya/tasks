package by.training.ifaces;

import by.training.constants.ConstantsJSP;
import by.training.model.beans.Book;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The servlet contains common methods for all servlets
 */
public class BaseServlet extends HttpServlet {
    private static final String JUMP_INDEX = "/index.jsp";
    private static final String KEY_MESSAGE_DO_ACTION = "messageAction";
    private static final String MESSAGE_IF_PARAM_NULL = "Enter the correct data";
    protected static final String PATTERN_DATE = "yyyy-MM-dd_HH-mm-ss";

    protected String messageDoAction;

    protected void forward(final String url, final HttpServletRequest request,
                           final HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(url);
        requestDispatcher.forward(request, response);
    }

    protected void forwardError(final String message, final HttpServletRequest request,
                                final HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute(KEY_MESSAGE_DO_ACTION, message);
        forward(JUMP_INDEX, request, response);
    }

    protected void forwardError(final String message, final String url, final HttpServletRequest request,
                                final HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute(KEY_MESSAGE_DO_ACTION, message);
        forward(url, request, response);
    }

    protected void checkValueParam(final HttpServletRequest request, final HttpServletResponse response,
                                   final String... params) throws ServletException, IOException {
        for (String s : params) {
            if ("".equals(s)) {
                forwardError(MESSAGE_IF_PARAM_NULL, request, response);
            }
        }
    }

    protected Book getBookFromRequest(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        return getBookFromRequest(request, response, ConstantsJSP.KEY_AUTHOR,
                ConstantsJSP.KEY_TITLE_BOOK, ConstantsJSP.KEY_DESCRIPTION_BOOK);
    }

    protected Book getBookFromRequest(final HttpServletRequest request, final HttpServletResponse response,
                                      final String author, final String title, final String descrptoin)
            throws ServletException, IOException {
        String authorBook = request.getParameter(author);
        String titleBook = request.getParameter(title);
        String descriptionBook = request.getParameter(descrptoin);

        checkValueParam(request, response, author, titleBook);

        Book book = new Book();
        book.setAuthor(authorBook);
        book.setTitle(titleBook);
        book.setDescription(descriptionBook);

        return book;
    }

}
