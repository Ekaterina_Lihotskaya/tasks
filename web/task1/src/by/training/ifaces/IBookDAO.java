package by.training.ifaces;

import by.training.model.beans.Book;

/**
 * Contract for a DAO Book
 */
public interface IBookDAO {

    /**
     * Returns the specified book
     * @param book the book get from DB
     * @return book
     */
    Book getBook(Book book);

    /**
     * Update information about the book
     * @param updateBook updated information about the book
     */
    void updateBook(Book updateBook);

    /**
     * Add a book
     * @param book the book add in DB
     */
    void addBook(Book book);

    /**
     * Delete a book
     * @param id id of book to delete
     */
    void deleteBook(Integer idBook);
}
