package by.training.constants;

public class ConstantsJSP {
    public static final String KEY_ID_BOOK = "idBook";
    public static final String KEY_AUTHOR = "author";
    public static final String KEY_TITLE_BOOK = "title";
    public static final String KEY_DESCRIPTION_BOOK = "description";

    public static final String KEY_AUTHOR_UPDATE = "updateAuthor";
    public static final String KEY_TITLE_BOOK_UPDATE = "updateTitle";
    public static final String KEY_DESCRIPTION_BOOK_UPDATE = "updateDescription";

}