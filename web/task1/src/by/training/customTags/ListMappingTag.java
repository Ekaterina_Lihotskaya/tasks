package by.training.customTags;
import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ListMappingTag extends BodyTagSupport {
    private Map<String, ? extends ServletRegistration> registrations;
    private List<String> nameServlets;

    @Override
    public int doStartTag() throws JspException {
        ServletContext servletContext = pageContext.getServletContext();
        registrations = servletContext.getServletRegistrations();
        nameServlets = new ArrayList<>(registrations.keySet());
        return EVAL_BODY_INCLUDE;
    }

    @Override
    public int doAfterBody() throws JspException {
        if (!nameServlets.isEmpty()) {
            String servlet = nameServlets.get(0);
            pageContext.setAttribute("servlet", servlet);

            String url = registrations.get(servlet).getMappings().toString();
            pageContext.setAttribute("url", url);

            nameServlets.remove(0);
            return EVAL_BODY_AGAIN;
        }
        return SKIP_BODY;
    }
}
