package by.training.customTags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;


public class ResolveUrlTag extends SimpleTagSupport {
    private String url;

    @Override
    public void doTag() throws JspException, IOException {
        PageContext pageContext = (PageContext) getJspContext();
        pageContext.setAttribute("url", url);

        String resource = pageContext.getServletContext().getRealPath(url);
        pageContext.setAttribute("resource", resource);

        getJspBody().invoke(null);
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
